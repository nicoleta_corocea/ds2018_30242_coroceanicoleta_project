drop database fitnessDB;
create database fitnessDB;

use fitnessDB;

CREATE TABLE subscriptions (
	id int,
    activation_date long not null,
    expiration_date long not null,
    subscription_type varchar(20) not null,
    disabled boolean not null,
    PRIMARY KEY (id)
);

CREATE TABLE users (
    id int auto_increment,
    first_name varchar(20) NOT NULL,
    last_name varchar(20) NOT NULL,
    email varchar(50) NOT NULL,
    password varchar(256) NOT NULL,
    id_subscription int,
    status varchar(10) not null,
    PRIMARY KEY (id), 
    foreign key (id_subscription) references subscriptions(id)
);

CREATE TABLE roles (
	id int,
    role varchar(15) not null,
    PRIMARY KEY (id)
);

CREATE TABLE user_role (
	id_user int not null,
    id_role int not null,
    PRIMARY KEY(id_user, id_role),
    FOREIGN KEY (id_user) REFERENCES users(id),
    FOREIGN KEY (id_role) REFERENCES roles(id)
);

CREATE TABLE activities (
	id int auto_increment,
    id_trainer int,
    title varchar(20) not null,
    start_date long not null,
    duration long not null,
    available_seats int not null,
    remaining_seats int not null,
    available boolean not null,
    type varchar(30) not null,
    FOREIGN KEY (id_trainer) REFERENCES users(id),
    primary key(id)
);

CREATE TABLE appointments (
    id_activity int not null,
    id_user int not null,
	FOREIGN KEY (id_activity) REFERENCES activities(id),
	FOREIGN KEY (id_user) REFERENCES users(id),
    primary key(id_activity, id_user)
);

CREATE TABLE ratings (
	id_user int not null,
    id_trainer int not null,
    rate float not null,
    PRIMARY KEY(id_user, id_trainer),
    FOREIGN KEY (id_user) REFERENCES users(id),
    FOREIGN KEY (id_trainer) REFERENCES users(id)
);

INSERT INTO `roles` (`id`,`role`) VALUES (1, 'ADMIN'); 
INSERT INTO `roles` (`id`,`role`) VALUES (2, 'TRAINER'); 
INSERT INTO `roles` (`id`,`role`) VALUES (3, 'CLIENT'); 

INSERT INTO `users` (`first_name`,`last_name`,`email`,`password`,`status`) VALUES ('admin','admin','admin@admin.com','admin',''); 
INSERT INTO `user_role` (`id_user`,`id_role`) VALUES (1,1);

INSERT INTO `subscriptions` (`id`,`activation_date`,`expiration_date`,`subscription_type`,`disabled`) VALUES (2,1543968000,1546646400,'VIP',FALSE); 
INSERT INTO `users` (`first_name`,`last_name`,`email`,`password`,`id_subscription`,`status`) VALUES  ('Nicoleta','Corocea','nicoleta_corocea@yahoo.com','parola',2,'ACTIVE'); 
INSERT INTO `users` (`first_name`,`last_name`,`email`,`password`,`status`) VALUES ('Adrian','Popescu','nicoletacorocea@gmail.com','parola1','ACTIVE'); 
INSERT INTO `users` (`first_name`,`last_name`,`email`,`password`,`status`) VALUES ('Nelu','Lala','nelulala@gmail.comcom','parola1','ACTIVE'); 

INSERT INTO `user_role` (`id_user`,`id_role`) VALUES (2,3);
INSERT INTO `user_role` (`id_user`,`id_role`) VALUES (3,2);

# positive test:

INSERT INTO `activities` (`id_trainer`, `title`, `start_date`, `duration`, `available_seats`, `remaining_seats`,`available`, `type`) VALUES (3,'zumba',1543536000, 30, 30, 20,true, 'CLASS');
INSERT INTO `appointments` (`id_activity`,`id_user`) VALUES (1,16);



#---------------------------------------------------------------------------

INSERT INTO `users` (`first_name`,`last_name`,`email`,`password`,`status`) VALUES ('Cristi','Palacean','cristi@yahoo.com','parola','ACTIVE'); 


INSERT INTO `user_role` (`id_user`,`id_role`) VALUES (2,2);
INSERT INTO `user_role` (`id_user`,`id_role`) VALUES (3,3);

INSERT INTO `activities` (`id_trainer`, `title`, `start_date`, `duration`, `available_seats`, `remaining_seats`,`available`, `type`) VALUES (2,'zumba',1543152, 30, 30, 20,true, 'CLASS');
INSERT INTO `activities` (`id_trainer`, `title`, `start_date`, `duration`, `available_seats`, `remaining_seats`, `available` , `type`) VALUES (2,'aerobic',1544572800, 30, 30, 20, true,'CLASS');
INSERT INTO `activities` (`id_trainer`, `title`, `start_date`, `duration`, `available_seats`, `remaining_seats`, `available` , `type`) VALUES (2,'pt',1544572800, 30, 2, 1, true,'PERSONAL_TRAINING');
INSERT INTO `activities` (`id_trainer`, `title`, `start_date`, `duration`, `available_seats`, `remaining_seats`, `available` , `type`) VALUES (5,'pt',1545858872, 30, 2, 1, true,'PERSONAL_TRAINING');

INSERT INTO `appointments` (`id_activity`,`id_user`) VALUES (1,3);
INSERT INTO `appointments` (`id_activity`,`id_user`) VALUES (2,3);

INSERT INTO `subscriptions` (`id`,`activation_date`,`expiration_date`,`subscription_type`,`disabled`) VALUES (1,111111,222222,'VIP',FALSE); 

INSERT INTO `ratings` (`id_user`,`id_trainer`,`rate`) values (2,17,4.5);
INSERT INTO `ratings` (`id_user`,`id_trainer`,`rate`) values (16,15,4.8);
INSERT INTO `ratings` (`id_user`,`id_trainer`,`rate`) values (2,15,4.9);

delete from subscriptions where id=4;
delete from ratings where id_trainer=5;
delete from appointments where id_activity=4;
delete from activities where id=4;

update users set password='admin' where id=1;





select * from users;
select * from user_role;
select * from activities;
select * from appointments;
select * from roles;
select * from ratings;
select * from subscriptions;