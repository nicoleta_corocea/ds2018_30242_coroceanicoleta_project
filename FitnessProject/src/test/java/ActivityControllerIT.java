
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

import sd.fitness.App;
import sd.fitness.controller.ActivityController;
import sd.fitness.manager.ActivityManager;
import sd.fitness.model.Activity;
import sd.fitness.model.User;

@SpringBootTest(classes = App.class)
@RunWith(SpringRunner.class)
public class ActivityControllerIT {
	MockMvc mockMvc;

	@InjectMocks
	ActivityController activityController;

	@Mock
	ActivityManager activityManager;

	@Before
	public void setup() throws Exception {
		this.mockMvc = standaloneSetup(this.activityController).build();// Standalone context
	}

	@Test
	public void getActivityById_Test() throws Exception {
		int id = 1;
		when(activityManager.getActivityById(id)).thenReturn(getActivity());
		mockMvc.perform(get("/activities/{id}", id))

				.andExpect(status().isOk()).andExpect(jsonPath("$.title").value("aerobic"))
				.andExpect(jsonPath("$.duration").value(new Long(30)))
				.andExpect(jsonPath("$.type").value("PERSONAL_TRAINING"))
				.andExpect(jsonPath("$.availableSeats").value(new Long(20)))
				.andExpect(jsonPath("$.trainer.id").value(3));
	}

	@Test
	public void addActivity_test() throws Exception {

		when(activityManager.addActivity(getActivity())).thenReturn(getActivity());
		ObjectMapper mapper = new ObjectMapper();
		String requestJson = mapper.writeValueAsString(getActivity());

		this.mockMvc.perform(post("/activities/add").contentType(MediaType.APPLICATION_JSON).content(requestJson))
				.andDo(print()).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$.title").value("aerobic")).andExpect(jsonPath("$.duration").value(new Long(30)))
				.andExpect(jsonPath("$.type").value("PERSONAL_TRAINING"))
				.andExpect(jsonPath("$.availableSeats").value(new Long(20)))
				.andExpect(jsonPath("$.trainer.id").value(3));
	}

	@Test
	public void editActivity_test() throws Exception {

		Activity newActivity = getActivity();
		newActivity.setTitle("zumba");
		when(activityManager.editActivity(newActivity)).thenReturn(newActivity);

		ObjectMapper mapper = new ObjectMapper();
		String requestJson = mapper.writeValueAsString(newActivity);

		this.mockMvc.perform(put("/activities/edit").contentType(MediaType.APPLICATION_JSON).content(requestJson))
				.andDo(print()).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$.title").value("zumba")).andExpect(jsonPath("$.duration").value(new Long(30)))
				.andExpect(jsonPath("$.type").value("PERSONAL_TRAINING"))
				.andExpect(jsonPath("$.availableSeats").value(new Long(20)))
				.andExpect(jsonPath("$.trainer.id").value(3));
	}

	private Activity getActivity() {
		Activity activity = new Activity();
		activity.setTitle("aerobic");
		activity.setStartDate(new Long(1544744800));
		activity.setDuration(new Long(30));
		activity.setAvailableSeats(20);
		activity.setRemainingSeats(20);
		activity.setType("PERSONAL_TRAINING");
		User trainer = new User();
		trainer.setId(3);
		activity.setTrainer(trainer);
		return activity;
	}

}
