
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

import sd.fitness.App;
import sd.fitness.controller.UserController;
import sd.fitness.manager.UserManager;
import sd.fitness.model.User;
@Ignore
@SpringBootTest(classes = App.class)
@RunWith(SpringRunner.class)
public class UserControllerIT {
	MockMvc mockMvc;

	@InjectMocks
	UserController userController;

	@Mock
	UserManager userManager;

	@Before
	public void setup() throws Exception {
		this.mockMvc = standaloneSetup(this.userController).build();// Standalone context
	}

	@Test
	public void getUserById_Test() throws Exception {
		int id = 1;
		when(userManager.getUserById(id)).thenReturn(getAdmin());
		mockMvc.perform(get("/users/{id}", id))

				.andExpect(status().isOk()).andExpect(jsonPath("$.email").value("admin@admin.com"))
				.andExpect(jsonPath("$.firstName").value("admin")).andExpect(jsonPath("$.lastName").value("admin"))
				.andExpect(jsonPath("$.password").value("admin"));
	}

	@Test
	public void addUser_test() throws Exception {

		when(userManager.insertUser(getUser())).thenReturn(getUser());
		ObjectMapper mapper = new ObjectMapper();
		String requestJson = mapper.writeValueAsString(getUser());

		this.mockMvc.perform(post("/users/add").contentType(MediaType.APPLICATION_JSON).content(requestJson))
				.andDo(print()).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$.email").value("test@test.com")).andExpect(jsonPath("$.firstName").value("test"))
				.andExpect(jsonPath("$.lastName").value("test")).andExpect(jsonPath("$.password").value("test"));

	}

	@Test
	public void editUser_test() throws Exception {

		User newUser = getUser();
		newUser.setEmail("test@yahoo.com");
		when(userManager.edit(newUser)).thenReturn(newUser);

		ObjectMapper mapper = new ObjectMapper();
		String requestJson = mapper.writeValueAsString(newUser);

		this.mockMvc.perform(put("/users/update").contentType(MediaType.APPLICATION_JSON).content(requestJson))
				.andDo(print()).andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$.email").value("test@yahoo.com")).andExpect(jsonPath("$.firstName").value("test"))
				.andExpect(jsonPath("$.lastName").value("test")).andExpect(jsonPath("$.password").value("test"));
	}

	private User getAdmin() {
		User admin = new User();
		admin.setEmail("admin@admin.com");
		admin.setFirstName("admin");
		admin.setLastName("admin");
		admin.setPassword("admin");
		return admin;
	}

	private User getUser() {
		User user = new User();
		user.setEmail("test@test.com");
		user.setFirstName("test");
		user.setLastName("test");
		user.setPassword("test");
		return user;
	}

}
