package sd.fitness.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import sd.fitness.config.exception.BusinessException;
import sd.fitness.manager.AppointmentManager;
import sd.fitness.model.Appointment;

@RestController
@RequestMapping("/appointments")
@CrossOrigin(origins = "http://localhost:4200")
public class AppointmentController {

	@Autowired
	private AppointmentManager appointmentManager;

	@PostMapping(path = "/add")
	public Appointment addAppointment(@RequestParam(value = "userId") int userId,
			@RequestParam(value = "activityId") int activityId) throws BusinessException {

		return appointmentManager.addAppointment(userId, activityId);
	}

	@DeleteMapping(path = "/delete")
	public Appointment deleteAppointment(@RequestParam("activityId") int activityId, @RequestParam("userId") int userId)
			throws BusinessException {
		return appointmentManager.deleteAppointment(activityId, userId);
	}

	@GetMapping(path = "/all")
	public List<Appointment> getAllAppointments() {
		return appointmentManager.getAllAppointments();
	}
}
