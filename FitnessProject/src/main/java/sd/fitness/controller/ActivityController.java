package sd.fitness.controller;

import java.util.List;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import sd.fitness.config.exception.BusinessException;
import sd.fitness.manager.ActivityManager;
import sd.fitness.model.Activity;

@RestController
@RequestMapping("/activities")
@Api(value="/activities",description="Activities",produces ="application/json")
@CrossOrigin(origins = "http://localhost:4200")
public class ActivityController {

	@Autowired
	private ActivityManager activityManager;

	@GetMapping("/all")
	public List<Activity> getAllActivities(@RequestParam(value = "future", required = false) Boolean future) {
		return activityManager.getAllActivities(future);
	}

	@GetMapping("/user/{userId}")
	public List<Activity> getActivitiesByUserId(@PathVariable("userId") int userId,
			@RequestParam(value = "available", required = false) Boolean available) throws BusinessException {
		return activityManager.getActivitiesByUserId(userId, available);
	}

	@GetMapping("/{activityId}")
	public Activity getActivityById(@PathVariable("activityId") int activityId) throws BusinessException {
		return activityManager.getActivityById(activityId);
	}

	@PostMapping("/add")
	public Activity addActivity(@RequestBody Activity activity) throws BusinessException {
		return activityManager.addActivity(activity);
	}

	@PutMapping("/disable/{activityId}")
	public Activity disableActivity(@PathVariable("activityId") int activityId) throws BusinessException {
		return activityManager.disableActivity(activityId);
	}

	@PutMapping(value="/edit")
	public Activity editActivity(@RequestBody Activity activity) throws BusinessException {
		return activityManager.editActivity(activity);
	}
}
