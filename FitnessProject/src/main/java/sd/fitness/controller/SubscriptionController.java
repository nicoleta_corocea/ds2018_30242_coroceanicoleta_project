package sd.fitness.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import sd.fitness.config.exception.BusinessException;
import sd.fitness.manager.SubscriptionManager;
import sd.fitness.model.Subscription;

@RestController
@RequestMapping("/subscriptions")
@CrossOrigin(origins = "http://localhost:4200")
public class SubscriptionController {

	@Autowired
	private SubscriptionManager subscriptionManager;

	@GetMapping("/all")
	public List<Subscription> getAllSubscriptions() {
		return subscriptionManager.getAllSubscriptions();
	}

	// TODO: to be checked
	@GetMapping("/{id}")
	public Subscription getSubscriptionById(@PathVariable("id") int id) throws BusinessException {
		return subscriptionManager.getSubscriptionById(id);
	}

	@PostMapping(path = "/add", consumes = "application/json", produces = "application/json")
	public Subscription addSubscription(@RequestBody Subscription subscription) throws BusinessException {
		return subscriptionManager.addSubscription(subscription);
	}

	@PutMapping(path = "/disable")
	public Subscription disableSubscription(@RequestParam("idSubscription") int idSubscription)
			throws BusinessException {
		return subscriptionManager.disableSubscription(idSubscription);
	}

	@PutMapping(path = "/edit", consumes = "application/json")
	public Subscription editSubscription(@RequestBody Subscription subscription) throws BusinessException {
		return subscriptionManager.editSubscription(subscription);
	}
}
