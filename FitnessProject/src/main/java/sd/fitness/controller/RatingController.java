package sd.fitness.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import sd.fitness.config.exception.BusinessException;
import sd.fitness.manager.RatingManager;
import sd.fitness.model.Rating;

@RestController
@RequestMapping("/ratings")
@CrossOrigin(origins = "http://localhost:4200")
public class RatingController {

	@Autowired
	private RatingManager ratingManager;

	@GetMapping("/all")
	public List<Rating> getAllRatings() {
		return ratingManager.getAllRatings();
	}

	@GetMapping("/computed/{trainerId}")
	public double getRatingByTrainerId(@PathVariable("trainerId") int trainerId) throws BusinessException {
		return ratingManager.getRatingByTrainerId(trainerId);
	}

	@GetMapping("/computed/all")
	public Map<Integer, Double> getAllComputedRatings() throws BusinessException {
		return ratingManager.getComputedRatings();
	}

	@PostMapping("/add")
	public Rating giveRating(@RequestBody Rating rating) throws BusinessException {
		return ratingManager.giveRating(rating);
	}

	@DeleteMapping("/delete")
	public Rating deleteRating(@RequestParam("userId") int userId, @RequestParam("trainerId") int trainerId)
			throws BusinessException {
		return ratingManager.deleteRating(userId, trainerId);
	}

	@PutMapping("/edit")
	public Rating editRating(@RequestBody Rating rating) throws BusinessException {
		return ratingManager.editRating(rating);
	}

}
