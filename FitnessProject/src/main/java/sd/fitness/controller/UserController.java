package sd.fitness.controller;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import sd.fitness.config.exception.BusinessException;
import sd.fitness.manager.UserManager;
import sd.fitness.model.User;
import sd.fitness.model.security.LoginCredentialsDTO;

@RestController
@RequestMapping("/users")
@CrossOrigin(origins = "http://localhost:4200")
public class UserController {

	@Autowired
	private UserManager userManager;

	@GetMapping("/all")
	public List<User> getAllUsers() {
		return userManager.getAllUsers();
	}

	@GetMapping("/{id}")
	public User getUserByID(@PathVariable("id") int id) throws BusinessException {
		return userManager.getUserById(id);
	}

	@GetMapping("/active")
	public List<User> getActiveUsersByRole(@RequestParam("role") String role) throws BusinessException {
		return userManager.getActiveUsersByRole(role);
	}

	@GetMapping("/{userId}/hasvalidsubscription")
	public boolean hasValidSubscription(@PathVariable("userId") int userId) {
		return userManager.hasValidSubscription(userId);
	}

	@GetMapping("/havevalidsubscription")
    public List<User> getUsersWithValidSubscription(){
	    return userManager.getUsersWithValidSubscription();
    }

	@GetMapping("/trainers")
	public Set<User> getTrainers(@RequestParam(value = "userId", required = false) Integer userId,
			@RequestParam(value = "hasAppointment", required = false) Boolean hasAppointment) throws BusinessException {
		return new HashSet<>(userManager.getTrainers(userId, hasAppointment));
	}

	@PostMapping(path = "/add", consumes = "application/json", produces = "application/json")
	public User addMember(@RequestBody User user) throws BusinessException {
		return userManager.insertUser(user);
	}

	@PostMapping("/{id}/changestatus")
	public User changeStatus(@PathVariable("id") int id, @RequestParam("status") String status)
			throws BusinessException {
		return userManager.changeStatus(id, status);
	}

	@PostMapping(path = "/login", consumes = "application/json", produces = "application/json")
	public User login(@RequestBody LoginCredentialsDTO loginCredentialsDTO) throws BusinessException {
		return userManager.login(loginCredentialsDTO);
	}

	@PutMapping(path = "/update")
	public User edit(@RequestBody User updateUser) throws BusinessException {
		return userManager.edit(updateUser);
	}

}
