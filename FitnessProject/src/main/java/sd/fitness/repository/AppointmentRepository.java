package sd.fitness.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import sd.fitness.model.Activity;
import sd.fitness.model.Appointment;
import sd.fitness.model.AppointmentId;

public interface AppointmentRepository extends CrudRepository<Appointment, AppointmentId> {

	List<Appointment> findByAppointmentIdActivity(Activity a);

	List<Appointment> findByAppointmentIdUserId(int id);

	Appointment findByAppointmentIdAndAppointmentIdUserId(int userId, int appointmentId);

	Appointment findByAppointmentIdActivityIdAndAppointmentIdUserId(int activityId, int userId);

}
