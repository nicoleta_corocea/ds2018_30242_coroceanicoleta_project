package sd.fitness.repository;

import java.util.Set;

import org.springframework.data.repository.CrudRepository;

import sd.fitness.model.Role;

public interface RoleRepository extends CrudRepository<Role, Integer> {

	Role findByRole(String role);

}
