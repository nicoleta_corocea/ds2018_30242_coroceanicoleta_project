package sd.fitness.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import sd.fitness.model.Activity;

public interface ActivityRepository extends CrudRepository<Activity, Integer> {

	public List<Activity> findByStartDateGreaterThanEqual(long today);

	public List<Activity> findByStartDateLessThanEqual(long today);

	public List<Activity> findByTrainerId(int id);

	public List<Activity> findByTrainerIdAndAvailable(int id, boolean available);

	public List<Activity> findAllByTypeAndAvailable(String string, boolean available);
}
