package sd.fitness.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import sd.fitness.model.Rating;
import sd.fitness.model.RatingId;

public interface RatingRepository extends CrudRepository<Rating, RatingId> {

	List<Rating> findByRatingIdTrainerId(int trainerId);

	Rating findByRatingIdUserIdAndRatingIdTrainerId(int userId, int trainerId);

}
