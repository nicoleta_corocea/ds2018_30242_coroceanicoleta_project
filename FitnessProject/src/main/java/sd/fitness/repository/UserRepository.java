package sd.fitness.repository;

import java.util.List;
import java.util.Set;

import org.springframework.data.repository.CrudRepository;

import sd.fitness.model.Role;
import sd.fitness.model.User;

public interface UserRepository extends CrudRepository<User, Integer> {

	User findByEmail(String email);

	List<User> findByStatusAndRoles(String string, Set<Role> roles);


	
}
