package sd.fitness.repository;

import org.springframework.data.repository.CrudRepository;

import sd.fitness.model.Subscription;

public interface SubscriptionRepository extends CrudRepository<Subscription, Integer>{

}
