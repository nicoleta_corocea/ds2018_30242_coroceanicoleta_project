package sd.fitness.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.access.channel.ChannelProcessingFilter;
import sd.fitness.service.security.CorsFilter;
import sd.fitness.service.security.CustomUserDetailsServiceImpl;

@Configuration
@EnableWebSecurity//(debug = true)
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)

public class SecurityConfig extends WebSecurityConfigurerAdapter {

//	@Override
//	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//		auth.userDetailsService(createUserDetailsService()).passwordEncoder(passwordEncoder());
//	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);


		http.addFilterBefore(new CorsFilter(), ChannelProcessingFilter.class);

		http
				.authorizeRequests()
				.antMatchers("/users/login").permitAll()
				.antMatchers("/activities/all","/activities/user/{\\d+}","/users/update").hasAnyAuthority("ROLE_TRAINER", "ROLE_CLIENT","ROLE_ADMIN")
				.and().authorizeRequests()
				.antMatchers("/users/{\\d+}/changestatus","users/havevalidsubscription","/subscriptions/all","/subscriptions/add","/subscriptions/disable","/subscriptions/edit","/appointments/all").hasAnyAuthority("ROLE_ADMIN")
				.antMatchers("/activities/add","/activities/disable/{\\d+}","/activities/edit").hasAnyAuthority("ROLE_TRAINER")
				.antMatchers("/appointments/add","/appointments/delete","/ratings/add","/ratings/delete","/ratings/edit").hasAnyAuthority("ROLE_CLIENT")
				.antMatchers("/users/{\\d+}","/users/{\\d+}/hasvalidsubscription","/users/trainers","/subscriptions/{\\d+}","/ratings/all","/ratings/computed/all").hasAnyAuthority("ROLE_ADMIN", "ROLE_CLIENT")
				.antMatchers("/activities/{\\d+}","/ratings/computed/{\\d+}").hasAnyAuthority("ROLE_TRAINER", "ROLE_CLIENT")
				.and().formLogin().disable().httpBasic()
				.and()
				.httpBasic()
				.and().csrf().disable();


	}

	@Override
	public void configure(WebSecurity web) throws Exception {
		/* To allow Pre-flight [OPTIONS] request from browser */
		web.ignoring().antMatchers(HttpMethod.OPTIONS, "/**");
	}

	/**
	 * Create password encoder bean used for encrypting the password
	 *
	 * @return
	 */
	@Bean
	public PasswordEncoder passwordEncoder() {
		return new PasswordEncoder() {
			@Override
			public String encode(CharSequence rawPassword) {
				return (String) rawPassword;
			}

			@Override
			public boolean matches(CharSequence rawPassword, String encodedPassword) {
				if ( encodedPassword.equals(rawPassword)){
					return true;
				}
				return false;
			}
		};
	}

	/**
	 * Create user service bean used for find the user by email
	 *
	 * @return
	 */
	@Bean
	public UserDetailsService createUserDetailsService() {
		return new CustomUserDetailsServiceImpl();
	}
}
