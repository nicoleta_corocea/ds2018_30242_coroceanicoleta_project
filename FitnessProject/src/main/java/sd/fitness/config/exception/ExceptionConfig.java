package sd.fitness.config.exception;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExceptionConfig {
	
	@ExceptionHandler(BusinessException.class)
	public ResponseEntity parseBusinessException(BusinessException ex) {
		Map<String, Object> responseMessage = new LinkedHashMap<>();
		responseMessage.put("status", ex.getStatus());
		responseMessage.put("message", ex.getMessage());

		return new ResponseEntity<>(responseMessage, HttpStatus.valueOf(ex.getStatus()));
	}
}
