package sd.fitness.util;

import org.apache.commons.lang.RandomStringUtils;

public class PasswordGenerator {

	public static String generate() {
		String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789~`!@#$%^&*()-_=+[{]}\\|;:\'\",<.>/?";
		String pwd = RandomStringUtils.random(15, characters);
		System.out.println(pwd);
		return pwd;
	}
}
