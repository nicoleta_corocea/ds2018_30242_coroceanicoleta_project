package sd.fitness.model;

public enum UserStatus {
	ACTIVE, DELETED;
}
