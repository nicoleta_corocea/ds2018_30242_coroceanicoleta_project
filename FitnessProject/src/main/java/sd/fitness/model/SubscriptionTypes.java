package sd.fitness.model;

public enum SubscriptionTypes {
	VIP, STANDARD, FITNESS, AEROBIC;
}
