package sd.fitness.model;

public enum RoleType {
	ADMIN, TRAINER, CLIENT;
}
