package sd.fitness.model;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "appointments")
public class Appointment {

	@EmbeddedId
	private AppointmentId appointmentId;

	public Appointment() {
	}

	public Appointment(AppointmentId appointmentId) {
		this.appointmentId = appointmentId;
	}

	public AppointmentId getAppointmentId() {
		return appointmentId;
	}

	public void setAppointmentId(AppointmentId appointmentId) {
		this.appointmentId = appointmentId;
	}

}
