package sd.fitness.service;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sd.fitness.config.exception.BusinessException;
import sd.fitness.model.Appointment;
import sd.fitness.model.Role;
import sd.fitness.model.RoleType;
import sd.fitness.model.Subscription;
import sd.fitness.model.User;
import sd.fitness.model.UserStatus;
import sd.fitness.model.security.LoginCredentialsDTO;
import sd.fitness.repository.AppointmentRepository;
import sd.fitness.repository.RoleRepository;
import sd.fitness.repository.SubscriptionRepository;
import sd.fitness.repository.UserRepository;
import sd.fitness.util.PasswordGenerator;
import sd.fitness.validator.UserValidator;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private SubscriptionRepository subscriptionRepository;

	@Autowired
	private AppointmentRepository appointmentRepository;

	@Autowired
	private UserValidator userValidator;

	@Override
	public List<User> getAllUsers() {
		return (List<User>) userRepository.findAll();
	}

	@Override
	public User insertUser(User user) throws BusinessException {
		userValidator.checkValid(user, false);
		Set<Role> roleList = new HashSet<>();
		if (user.getRoles() != null) {
			for (Role r : user.getRoles()) {
				userValidator.checkRoleExistance(r.getRole());
				roleList.add(roleRepository.findById(r.getId()).get());
			}
		} else {
			throw new BusinessException(400, "Missing role for user to be inserted.");
		}

		User existingUser = userRepository.findByEmail(user.getEmail());
		if (existingUser != null) {
			throw new BusinessException(409, "Email is already in use.");
		}
		User newUser = new User(user.getFirstName(), user.getLastName(), user.getEmail(), PasswordGenerator.generate(),
				roleList, UserStatus.ACTIVE.toString());
		User savedUser = userRepository.save(newUser);

		sendEmail(user.getEmail(), "Bine ai venit!", "Contul tau este: \n Email: " + savedUser.getEmail() + "\nParola: "
				+ savedUser.getPassword() + "\n Parola o poti schimba oricand din contul tau.");

		return savedUser;
	}

	@Override
	public User login(LoginCredentialsDTO loginCredentialsDTO) throws BusinessException {

		userValidator.loginValidator(loginCredentialsDTO);

		final User existingUser = userRepository.findByEmail(loginCredentialsDTO.getEmail());
		if (Objects.isNull(existingUser)) {
			throw new BusinessException(401, "Bad credentials");
		}

		if (Objects.isNull(existingUser)) {
			throw new BusinessException(401, "Bad credentials");
		}

		if (existingUser.getStatus().equals(UserStatus.DELETED.toString())) {
			throw new BusinessException(401, "Disabled User");
		}

		// TODO:
		/*
		 * if(!PasswordEncoder.matches(loginCredentialsDTO.getPassword(),
		 * existingUser.getPassword())) { throw new BusinessException(401,
		 * "Bad credentials"); }
		 */

		if (!loginCredentialsDTO.getPassword().equals(existingUser.getPassword())) {
			throw new BusinessException(401, "Bad credentials");
		}

		return existingUser;

	}

	@Override
	public User changeStatus(int id, String status) throws BusinessException {
		userValidator.checkStatusExistance(status);
		Optional<User> returnedUser = userRepository.findById(id);
		User userToBeChanged = null;
		if (returnedUser.isPresent()) {
			userToBeChanged = returnedUser.get();
		}

		if (userToBeChanged == null) {
			throw new BusinessException(404, "User not found");
		}

		userToBeChanged.setStatus(status);
		return userRepository.save(userToBeChanged);
	}

	@Override
	public List<User> getActiveUsersByRole(String role) throws BusinessException {
		if (role == null) {
			throw new BusinessException(400, "Missing role");
		}
		userValidator.checkRoleExistance(role);
		Set<Role> roleList = new HashSet<>();

		roleList.add(roleRepository.findByRole(role));
		List<User> activeUsersWithRole = userRepository.findByStatusAndRoles(UserStatus.ACTIVE.toString(), roleList);

		return activeUsersWithRole;
	}

	static void sendEmail(String receiver, String subject, String message) throws BusinessException {
		MailService mailService = new MailService("nicoale232@gmail.com", "proiectis");
		mailService.sendMail(receiver, subject, message);

	}

	@Override
	public boolean hasValidSubscription(int userId) {
		Subscription subscription = null;
		Optional<Subscription> optionalSubscription = subscriptionRepository.findById(userId);
		if (optionalSubscription.isPresent()) {
			subscription = optionalSubscription.get();
		}

		if (subscription == null) {
			return false;
		}
		if(subscription.getDisabled()){
			return false;
		}
		long currentTime = new Date().getTime() / 1000;
		long expirationTime = subscription.getExpirationDate();

		if (currentTime - expirationTime > 0) {
			return false;
		}
		return true;

	}

	@Override
	public User edit(User updateUser) throws BusinessException {
		userValidator.checkValid(updateUser, true);

		Optional<User> optionalUsers = userRepository.findById(updateUser.getId());
		if (!optionalUsers.isPresent()) {
			throw new BusinessException(404, "User not found");
		}

		User editedUser = optionalUsers.get();

		if (!editedUser.getEmail().equals(updateUser.getEmail())) {
			User existingUser = userRepository.findByEmail(updateUser.getEmail());
			if (existingUser != null) {
				throw new BusinessException(409, "Email is already in use.");
			}
		}

		editedUser.setEmail(updateUser.getEmail());
		editedUser.setFirstName(updateUser.getFirstName());
		editedUser.setLastName(updateUser.getLastName());
		if (!updateUser.getPassword().equals(editedUser.getPassword())) {
			editedUser.setPassword(updateUser.getPassword());
			sendEmail(updateUser.getEmail(), "Salut!",
					"Contul tau este: \n Email: " + updateUser.getEmail() + "\nParola: " + updateUser.getPassword());
		}
		editedUser.setSubscription(updateUser.getSubscription());

		return userRepository.save(editedUser);
	}

	/**
	 * get trainers for an userId who had an appointment (or not).
	 */
	@Override
	public Set<User> getTrainers(Integer userId, Boolean hasAppointment) throws BusinessException {
		if (userId == null && hasAppointment != null) {
			throw new BusinessException(400, "Can't process this request.");
		}

		Set<User> trainers = new HashSet<>();
		Set<Role> roleList = new HashSet<>();
		roleList.add(roleRepository.findByRole(RoleType.TRAINER.toString()));

		if (hasAppointment == null) {
			// get all trainers

			trainers = new HashSet<>(userRepository.findByStatusAndRoles(UserStatus.ACTIVE.toString(), roleList));
		}
		if (userId != null && hasAppointment != null) {
			// has app false
			if (hasAppointment.equals(false)) {
				trainers = substractTrainers((List<Appointment>) appointmentRepository.findAll(),
						appointmentRepository.findByAppointmentIdUserId(userId));
			}
			// has app true
			if (hasAppointment.equals(true)) {
				trainers = extractTrainers(appointmentRepository.findByAppointmentIdUserId(userId));
			}
		}
		return trainers;
	}

	private Set<User> extractTrainers(List<Appointment> appointments) {
		Set<User> trainers = new HashSet<>();
		for (Appointment a : appointments) {
			if(a.getAppointmentId().getActivity().getAvailable()) {
				trainers.add(a.getAppointmentId().getActivity().getTrainer());
			}
		}
		return trainers;
	}

	private Set<User> substractTrainers(List<Appointment> all, List<Appointment> usersApp) {
		Set<User> trainers = new HashSet<>();
		for (Appointment a : all) {
			if (!usersApp.contains(a)) {
				trainers.add(a.getAppointmentId().getActivity().getTrainer());
			}
		}
		return trainers;

	}

	@Override
	public User getUserById(int id) throws BusinessException {
		Optional<User> optionalUser = userRepository.findById(id);

		if (!optionalUser.isPresent()) {
			throw new BusinessException(404, "User not found");
		}
		return optionalUser.get();
	}
}
