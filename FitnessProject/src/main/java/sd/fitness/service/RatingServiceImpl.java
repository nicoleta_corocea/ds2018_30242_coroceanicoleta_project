package sd.fitness.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sd.fitness.config.exception.BusinessException;
import sd.fitness.model.Rating;
import sd.fitness.repository.RatingRepository;

@Service
public class RatingServiceImpl implements RatingService {

	@Autowired
	private RatingRepository ratingRepository;

	@Override
	public Rating giveRating(Rating rating) throws BusinessException {
		return ratingRepository.save(rating);
	}

	@Override
	public List<Rating> getRatingByTrainerId(int trainerId) {
		 return ratingRepository.findByRatingIdTrainerId(trainerId);
	}

	@Override
	public List<Rating> getAllRatings() {
		return (List<Rating>) ratingRepository.findAll();
	}

	@Override
	public Rating deleteRating(int userId, int trainerId) throws BusinessException {
		Rating rating = getRatingByUserIdAndTrainerId(userId, trainerId);
		ratingRepository.delete(rating);
		return rating;
	}

//	@Override
//	public Map<Integer, Double> getComputedRatings() {
//
//	}

	@Override
	public Rating editRating(Rating rating) throws BusinessException {
		Rating existingRating = getRatingByUserIdAndTrainerId(rating.getRatingId().getUserId(),
				rating.getRatingId().getTrainerId());

		if (rating.getRate() != null) {
			existingRating.setRate(rating.getRate());
		}

		return ratingRepository.save(existingRating);
	}

	@Override
	public Rating getRatingByUserIdAndTrainerId(int userId, int trainerId) throws BusinessException {
		Rating rating = ratingRepository.findByRatingIdUserIdAndRatingIdTrainerId(userId, trainerId);
		if (rating == null) {
			throw new BusinessException(404, "Rating to be deleted was not found");
		}

		return rating;
	}

}
