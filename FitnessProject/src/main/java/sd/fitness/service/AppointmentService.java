package sd.fitness.service;

import java.util.List;

import sd.fitness.config.exception.BusinessException;
import sd.fitness.model.Appointment;

public interface AppointmentService {

	Appointment addAppointment(int userId, int activityId) throws BusinessException;

	Appointment deleteAppointment(int activityId, int userId) throws BusinessException;

	List<Appointment> getAllAppointments();

}
