package sd.fitness.service;

import java.util.List;

import org.springframework.stereotype.Service;

import sd.fitness.config.exception.BusinessException;
import sd.fitness.model.Activity;

public interface ActivityService {

	List<Activity> getAllActivities(Boolean future);

	List<Activity> getActivitiesByUserId(int id, Boolean available) throws BusinessException;

	Activity decrementRemainingSeats(int activityId) throws BusinessException;

	boolean hasSeats(int activityId) throws BusinessException;

	Activity getActivityById(int activityId) throws BusinessException;

	Activity addActivity(Activity activity) throws BusinessException;

	boolean isAvailable(int activityId) throws BusinessException;

	Activity disableActivity(int activityId) throws BusinessException;

	Activity editActivity(Activity activity) throws BusinessException;

	List<Activity> selectOldActivitiesOfUser(int userId) throws BusinessException;

}
