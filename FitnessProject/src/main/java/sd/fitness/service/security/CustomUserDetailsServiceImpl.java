package sd.fitness.service.security;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import sd.fitness.model.User;
import sd.fitness.model.security.CustomUserDetails;
import sd.fitness.repository.UserRepository;

public class CustomUserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	private UserRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		final User user = userRepository.findByEmail(username);

		if (Objects.isNull(user)) {
			throw new UsernameNotFoundException("User not found");
		}

		return new CustomUserDetails(user);
	}

}
