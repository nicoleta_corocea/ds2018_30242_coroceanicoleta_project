package sd.fitness.service;

import java.util.List;
import java.util.Map;

import sd.fitness.config.exception.BusinessException;
import sd.fitness.model.Rating;

public interface RatingService {

	Rating giveRating(Rating rating) throws BusinessException;

	List<Rating> getRatingByTrainerId(int trainerId);

//	Map<Integer, Double> getComputedRatings();

	List<Rating> getAllRatings();

	Rating deleteRating(int userId, int trainerId) throws BusinessException;

	Rating editRating(Rating rating) throws BusinessException;

	Rating getRatingByUserIdAndTrainerId(int userId, int trainerId) throws BusinessException;
}
