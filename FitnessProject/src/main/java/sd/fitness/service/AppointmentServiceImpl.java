package sd.fitness.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sd.fitness.config.exception.BusinessException;
import sd.fitness.model.Activity;
import sd.fitness.model.Appointment;
import sd.fitness.model.AppointmentId;
import sd.fitness.model.User;
import sd.fitness.repository.ActivityRepository;
import sd.fitness.repository.AppointmentRepository;
import sd.fitness.repository.UserRepository;

@Service
public class AppointmentServiceImpl implements AppointmentService {

	@Autowired
	private AppointmentRepository appointmentRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private ActivityRepository activityRepository;

	@Override
	public Appointment addAppointment(int userId, int activityId) throws BusinessException {
		Appointment optionalAppointment = appointmentRepository
				.findByAppointmentIdActivityIdAndAppointmentIdUserId(activityId, userId);
		if (optionalAppointment != null) {
			throw new BusinessException(409, "Conflict. An appointment already exists");
		}

		Optional<User> optionalUser = userRepository.findById(userId);
		User user = null;
		if (optionalUser.isPresent()) {
			user = optionalUser.get();
		}

		Optional<Activity> optionalActivity = activityRepository.findById(activityId);
		Activity activity = null;
		if (optionalActivity.isPresent()) {
			activity = optionalActivity.get();
		}

		// activity.getParticipants().add(user);
		// TODO: what if they are null?
		// TODO: the activity should be saved with updated number of seats?
		if (activity.getStartDate() < (new Date().getTime() / 1000)) {
			throw new BusinessException(409, "Can't make an appointment to a past activity.");
		}
		Appointment appointment = null;
		if (activity != null && user != null) {
			appointmentRepository.save(new Appointment(new AppointmentId(user, activity)));
			appointment = appointmentRepository.findByAppointmentIdActivityIdAndAppointmentIdUserId(activityId, userId);
		} else {
			throw new BusinessException(400, "The appointment could not be saved");
		}
		return appointment;
	}

	@Override
	public Appointment deleteAppointment(int activityId, int userId) throws BusinessException {

		Appointment appointment = appointmentRepository.findByAppointmentIdActivityIdAndAppointmentIdUserId(activityId,
				userId);
		if (appointment == null) {
			throw new BusinessException(404, "Appointment not found");
		}
		Activity activity = activityRepository.findById(activityId).get();
		if(activity == null){
			throw new BusinessException(404, "Activity not found");
		}
		appointmentRepository.delete(appointment);
		activity.setRemainingSeats(activity.getRemainingSeats()+1);
		activityRepository.save(activity);
		return appointment;
	}

	@Override
	public List<Appointment> getAllAppointments() {
		return (List<Appointment>) appointmentRepository.findAll();
	}

}
