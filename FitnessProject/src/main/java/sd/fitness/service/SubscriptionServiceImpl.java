package sd.fitness.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sd.fitness.config.exception.BusinessException;
import sd.fitness.model.Subscription;
import sd.fitness.model.SubscriptionTypes;
import sd.fitness.repository.SubscriptionRepository;
import sd.fitness.repository.UserRepository;

@Service
public class SubscriptionServiceImpl implements SubscriptionService {

	@Autowired
	private SubscriptionRepository subscriptionRepository;

	@Autowired
	private UserRepository userRepository;

	@Override
	public Subscription addSubscription(Subscription subscription) throws BusinessException {
		return subscriptionRepository.save(subscription);
	}

	@Override
	public Subscription getSubscriptionById(int userId) throws BusinessException {
		Optional<Subscription> optionalSubscription = subscriptionRepository.findById(userId);
		if (!optionalSubscription.isPresent()) {
			throw new BusinessException(404, "User's subscription was not found");

		}
		return optionalSubscription.get();
	}

//TODO: to be checked, modified
	@Override
	public Subscription disableSubscription(int idSubscription) throws BusinessException {
		Subscription subscription = getSubscriptionById(idSubscription);
		subscription.setDisabled(true);
		Subscription savedSubscription = subscriptionRepository.save(subscription);

		return savedSubscription;
	}

	@Override
	public List<Subscription> getAllSubscriptions() {
		return (List<Subscription>) subscriptionRepository.findAll();
	}

	@Override
	public Subscription editSubscription(Subscription subscription) throws BusinessException {
		Subscription existingSubscription = getSubscriptionById(subscription.getId());

		existingSubscription.setActivationDate(subscription.getActivationDate());
		existingSubscription.setExpirationDate(subscription.getActivationDate() + 30 * 86400);// 30 days in seconds
		existingSubscription.setSubscriptionType(subscription.getSubscriptionType());

		return subscriptionRepository.save(existingSubscription);
	}

}
