package sd.fitness.service;

import java.util.List;
import java.util.Set;

import sd.fitness.config.exception.BusinessException;
import sd.fitness.model.User;
import sd.fitness.model.security.LoginCredentialsDTO;

public interface UserService {

	public List<User> getAllUsers();

	public User insertUser(User user) throws BusinessException;

	public User login(LoginCredentialsDTO loginCredentialsDTO) throws BusinessException;

	public User changeStatus(int id, String status) throws BusinessException;

	public List<User> getActiveUsersByRole(String role) throws BusinessException;

	public boolean hasValidSubscription(int userId);

	public User edit(User updateUser) throws BusinessException;

	public Set<User> getTrainers(Integer userId, Boolean hasAppointment) throws BusinessException;

	public User getUserById(int id) throws BusinessException;
}
