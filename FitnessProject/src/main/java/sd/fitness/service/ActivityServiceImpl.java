package sd.fitness.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sd.fitness.config.exception.BusinessException;
import sd.fitness.model.Activity;
import sd.fitness.model.ActivityType;
import sd.fitness.model.Appointment;
import sd.fitness.model.Role;
import sd.fitness.model.RoleType;
import sd.fitness.model.SubscriptionTypes;
import sd.fitness.model.User;
import sd.fitness.repository.ActivityRepository;
import sd.fitness.repository.AppointmentRepository;
import sd.fitness.repository.RoleRepository;
import sd.fitness.repository.UserRepository;
import sd.fitness.validator.ActivityValidator;

@Service
public class ActivityServiceImpl implements ActivityService {

	@Autowired
	private ActivityRepository activityRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private AppointmentRepository appointmentRepository;

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private ActivityValidator activityValidator;

	/**
	 * when available = true , returns future activities else returns all activities
	 */

	@Override
	public List<Activity> getAllActivities(Boolean future) {
		if (future == null) {
			return (List<Activity>) activityRepository.findAll();
		}

		List<Activity> activities = new ArrayList<>();

		if (future) {
			activities = (List<Activity>) activityRepository
					.findByStartDateGreaterThanEqual(new Date().getTime() / 1000);
		} else {
			activities = (List<Activity>) activityRepository.findByStartDateLessThanEqual(new Date().getTime() / 1000);
		}

		return activities;

	}

	/**
	 * If user role = client then get activities where user has appoinments.
	 * 
	 * If user role = trainer then: if available = true then return only available
	 * activites of trainer. Otherwise, if available = false, return only disabled
	 * activities of trainer
	 */
	@Override
	public List<Activity> getActivitiesByUserId(int id, Boolean available) throws BusinessException {
		List<Activity> activities = new ArrayList<>();

		Optional<User> userOptional = userRepository.findById(id);
		User user = null;
		if (userOptional.isPresent()) {
			user = userOptional.get();
		} else {
			throw new BusinessException(404, "User not found");
		}

		Role client = roleRepository.findByRole(RoleType.CLIENT.toString());
		if (user.getRoles().contains(client)) {
			List<Appointment> appointments = appointmentRepository.findByAppointmentIdUserId(id);
			for (Appointment a : appointments) {
				if (a.getAppointmentId().getActivity().getAvailable()) {
					activities.add(a.getAppointmentId().getActivity());
				}
			}
		}

		Role trainer = roleRepository.findByRole(RoleType.TRAINER.toString());
		if (user.getRoles().contains(trainer)) {
			if (available == null) {
				activities = (List<Activity>) activityRepository.findByTrainerId(user.getId());
			} else {
				activities = (List<Activity>) activityRepository.findByTrainerIdAndAvailable(user.getId(), available);
			}
		}

		return activities;

	}

	@Override
	public Activity decrementRemainingSeats(int activityId) throws BusinessException {
		Optional<Activity> optionalActivity = activityRepository.findById(activityId);
		if (!optionalActivity.isPresent()) {
			throw new BusinessException(404, "Activity not found.");
		}
		Activity activity = optionalActivity.get();
		activity.setRemainingSeats(activity.getRemainingSeats() - 1);
		Activity response = activityRepository.save(activity);

		return response;
	}

	@Override
	public boolean hasSeats(int activityId) throws BusinessException {
		Optional<Activity> optionalActivity = activityRepository.findById(activityId);
		if (!optionalActivity.isPresent()) {
			throw new BusinessException(404, "Activity not found.");
		}
		Activity activity = optionalActivity.get();

		if (activity.getRemainingSeats() > 0) {
			return true;
		}
		return false;

	}

	@Override
	public Activity getActivityById(int activityId) throws BusinessException {
		Optional<Activity> optionalActivity = activityRepository.findById(activityId);
		if (!optionalActivity.isPresent()) {
			throw new BusinessException(404, "Activity not found.");
		}

		return optionalActivity.get();
	}

	/**
	 * If activity is of type PERSONAL_TRAINING, then saves the activity. Otherwise,
	 * if the type is CLASS, checks if the activity can be added.
	 */
	@Override
	public Activity addActivity(Activity activity) throws BusinessException {

		activityValidator.checkValid(activity, false);

		Optional<User> optionalTrainer = userRepository.findById(activity.getTrainer().getId());
		if (!optionalTrainer.isPresent()) {
			throw new BusinessException(404, "Trainer not found");
		}

		activity.setTrainer(optionalTrainer.get());
		activity.setParticipants(new HashSet<User>());
		activity.setAvailable(true);

		if (activity.getType().equals(ActivityType.PERSONAL_TRAINING.toString())) {
			return activityRepository.save(activity);
		}

		List<Activity> activities = (List<Activity>) activityRepository
				.findAllByTypeAndAvailable(ActivityType.CLASS.toString(), true);
		activityValidator.canAddClassActivity(activities, activity);

		return activityRepository.save(activity);
	}

	@Override
	public boolean isAvailable(int activityId) throws BusinessException {
		Optional<Activity> optionalActivity = activityRepository.findById(activityId);
		if (!optionalActivity.isPresent()) {
			throw new BusinessException(404, "Activity not found.");
		}
		Activity activity = optionalActivity.get();
		return activity.getAvailable();
	}

	@Override
	public Activity disableActivity(int activityId) throws BusinessException {
		Optional<Activity> optionalActivity = activityRepository.findById(activityId);
		if (!optionalActivity.isPresent()) {
			throw new BusinessException(404, "Activity not found.");
		}
		Activity activity = optionalActivity.get();

		if (((new Date().getTime() / 1000) + 4 * 60 * 60) > activity.getStartDate()) {
			throw new BusinessException(409, "Can't disable this activity.");
		}

		activity.setAvailable(false);
		activityRepository.save(activity);

		for (User user : activity.getParticipants()) {
			sendEmail(user.getEmail(), "Salut!", "Activitatea programata la data "
					+ new Date(activity.getStartDate() * 1000) + "a fost anulata.\nMultumim pentru intelegere!");
		}
		return activity;
	}

	@Override
	public Activity editActivity(Activity newActivity) throws BusinessException {

		activityValidator.checkValid(newActivity, true);

		Optional<Activity> optionalActivity = activityRepository.findById(newActivity.getId());
		if (!optionalActivity.isPresent()) {
			throw new BusinessException(404, "Activity not found.");
		}
		Activity activity = optionalActivity.get();
		if (activity.getParticipants().size() != 0) {
			throw new BusinessException(409, "You can not update this activity");
		}

		activity.setAvailableSeats(newActivity.getAvailableSeats());
		activity.setDuration(newActivity.getDuration());
		activity.setRemainingSeats(newActivity.getRemainingSeats());
		activity.setStartDate(newActivity.getStartDate());
		activity.setTitle(newActivity.getTitle());
		activity.setType(newActivity.getType());

		if (activity.getType().equals(ActivityType.PERSONAL_TRAINING.toString())) {
			return activityRepository.save(activity);
		}

		List<Activity> activities = (List<Activity>) activityRepository
				.findAllByTypeAndAvailable(ActivityType.CLASS.toString(), true);
		
		activityValidator.canAddClassActivity(activities, activity);

		return activityRepository.save(activity);
	}

	static void sendEmail(String receiver, String subject, String message) throws BusinessException {
		MailService mailService = new MailService("nicoale232@gmail.com", "proiectis");
		mailService.sendMail(receiver, subject, message);
	}

	@Override
	public List<Activity> selectOldActivitiesOfUser(int userId) throws BusinessException {
		List<Activity> allActivities = getActivitiesByUserId(userId, true);
		List<Activity> oldActivities = new ArrayList<>();

		for (Activity a : allActivities) {
			if ((new Date().getTime() / 1000) > a.getStartDate()) {
				oldActivities.add(a);
			}
		}

		return oldActivities;
	}

}
