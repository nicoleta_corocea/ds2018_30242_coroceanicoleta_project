package sd.fitness.service;

import java.util.List;

import sd.fitness.config.exception.BusinessException;
import sd.fitness.model.Subscription;

public interface SubscriptionService {

	Subscription addSubscription(Subscription subscription) throws BusinessException;

	Subscription getSubscriptionById(int userId) throws BusinessException;

	Subscription disableSubscription(int idSubscription) throws BusinessException;

	List<Subscription> getAllSubscriptions();

	Subscription editSubscription(Subscription subscription) throws BusinessException;

}
