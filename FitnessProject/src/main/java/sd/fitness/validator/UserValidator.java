package sd.fitness.validator;

import java.util.Objects;

import org.springframework.stereotype.Component;

import sd.fitness.config.exception.BusinessException;
import sd.fitness.model.RoleType;
import sd.fitness.model.User;
import sd.fitness.model.UserStatus;
import sd.fitness.model.security.LoginCredentialsDTO;

@Component
public class UserValidator {

	public void checkValid(User user, boolean needsId) throws BusinessException {
		if (user == null) {
			throw new BusinessException(400, "Invalid user to be inserted");
		}
		if (needsId && (user.getId() == null || user.getPassword() == null)) {
			throw new BusinessException(400, "Missing user data.");
		}
		if (user.getEmail() == null || user.getFirstName() == null || user.getLastName() == null) {
			throw new BusinessException(400, "Missing data in user.");
		}
		if (!needsId && (user.getRoles() == null || user.getRoles().isEmpty())) {
			throw new BusinessException(400, "Missing data in user.");
		}
	}

	public void checkStatusExistance(String status) throws BusinessException {
		boolean isPresent = false;
		for (UserStatus s : UserStatus.values()) {
			if (s.toString().equals(status)) {
				isPresent = true;
			}
		}
		if (!isPresent) {
			throw new BusinessException(400, "Invalid user status.");
		}
	}

	public void checkRoleExistance(String role) throws BusinessException {
		boolean isPresent = false;
		for (RoleType r : RoleType.values()) {
			if (r.toString().equals(role)) {
				isPresent = true;
			}
		}
		if (!isPresent) {
			throw new BusinessException(400, "Invalid user role.");
		}
	}

	public void loginValidator(LoginCredentialsDTO loginCredentialsDTO) throws BusinessException {
		if (Objects.isNull(loginCredentialsDTO)) {
			throw new BusinessException(400, "Login credentials can not be null");
		}

		if (Objects.isNull(loginCredentialsDTO.getEmail())) {
			throw new BusinessException(400, "Email can not be null");
		}

		if (Objects.isNull(loginCredentialsDTO.getPassword())) {
			throw new BusinessException(400, "Password can not be null");
		}
	}
}
