package sd.fitness.validator;

import org.springframework.stereotype.Component;

import sd.fitness.config.exception.BusinessException;
import sd.fitness.model.Rating;

@Component
public class RatingValidator {

	public void checkValid(Rating rating) throws BusinessException {
		if (rating == null) {
			throw new BusinessException(400, "Invalid rating input.");
		}
		if (rating.getRatingId().getTrainerId() == null || rating.getRatingId().getUserId() == null
				|| rating.getRate() == null) {
			throw new BusinessException(400, "Missing rating data.");
		}

	}

}
