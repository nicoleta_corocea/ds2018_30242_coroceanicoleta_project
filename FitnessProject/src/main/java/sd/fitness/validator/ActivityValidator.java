package sd.fitness.validator;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;

import sd.fitness.config.exception.BusinessException;
import sd.fitness.model.Activity;
import sd.fitness.model.ActivityType;

@Component
public class ActivityValidator {

	public void checkValid(Activity activity, boolean needsId) throws BusinessException {
		if (activity == null) {
			throw new BusinessException(400, "Invalid input");
		}
		if (needsId && activity.getId() == null) {
			throw new BusinessException(400, "Can't process this request. Data is missing.");
		}
		if (activity.getTrainer() == null || activity.getStartDate() == null || activity.getDuration() == null
				|| activity.getTitle() == null || activity.getType() == null || activity.getAvailableSeats() == null) {
			throw new BusinessException(400, "Can't process this request. Data is missing.");
		}
		if (activity.getTrainer().getId() == null) {
			throw new BusinessException(400, "Can't process this request. The trainer id is missing.");
		}

		boolean isPresent = false;
		for (ActivityType a : ActivityType.values()) {
			if (a.toString().equals(activity.getType())) {
				isPresent = true;
			}
		}
		if (!isPresent) {
			throw new BusinessException(404, "Invalid activity type.");
		}

		if (activity.getStartDate() < (new Date().getTime() / 1000)) {
			throw new BusinessException(400, "Can't add activity in the past.");
		}
		if (activity.getAvailableSeats() <= 0 || activity.getRemainingSeats() < 0) {
			throw new BusinessException(400, "Can't add activity with negative seats.");
		}
	}

	public void canAddClassActivity(List<Activity> allActivities, Activity activity) throws BusinessException {
		for (Activity a : allActivities) {
			if (activity.getId() != null && a.getId().equals(activity.getId())) {
				continue;
			}
			if (((activity.getStartDate() >= a.getStartDate())
					&& (activity.getStartDate() <= (a.getStartDate() + a.getDuration() * 60))) //

					|| (((activity.getStartDate() + activity.getDuration() * 60) >= a.getStartDate()) && //
							((activity.getStartDate() + activity.getDuration() * 60) <= (a.getStartDate()
									+ a.getDuration() * 60)))//
					|| ((activity.getStartDate() <= a.getStartDate()) && (((activity.getStartDate()
							+ activity.getDuration() * 60) >= (a.getStartDate() + a.getDuration() * 60))))) {
				throw new BusinessException(409, "Can't add an activity of type CLASS at the selected time.");
			}
		}
	}
}
