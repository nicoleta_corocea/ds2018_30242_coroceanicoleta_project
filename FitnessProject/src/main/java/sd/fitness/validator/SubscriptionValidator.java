package sd.fitness.validator;

import org.springframework.stereotype.Component;

import sd.fitness.config.exception.BusinessException;
import sd.fitness.model.Subscription;
import sd.fitness.model.SubscriptionTypes;

@Component
public class SubscriptionValidator {

	public void checkValid(Subscription subscription) throws BusinessException {
		if (subscription == null) {
			throw new BusinessException(400, "Invalid subscription input.");
		}
		if (subscription.getId() == null || subscription.getActivationDate() == null
				|| subscription.getSubscriptionType() == null) {
			throw new BusinessException(400, "Missing input for adding subscription.");
		}
		boolean isPresent = false;
		for (SubscriptionTypes s : SubscriptionTypes.values()) {
			if (s.toString().equals(subscription.getSubscriptionType())) {
				isPresent = true;
			}
		}
		if (!isPresent) {
			throw new BusinessException(400, "Invalid subscription type.");
		}

	}

}
