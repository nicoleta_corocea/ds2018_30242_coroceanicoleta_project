package sd.fitness.manager;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sd.fitness.config.exception.BusinessException;
import sd.fitness.model.Activity;
import sd.fitness.service.ActivityService;

@Service
public class ActivityManager {

	@Autowired
	private ActivityService activityService;

	public List<Activity> getAllActivities(Boolean future) {
		return activityService.getAllActivities(future);
	}

	public List<Activity> getActivitiesByUserId(int id, Boolean available) throws BusinessException {
		return activityService.getActivitiesByUserId(id, available);
	}

	public Activity getActivityById(int activityId) throws BusinessException {
		return activityService.getActivityById(activityId);
	}

	public Activity addActivity(Activity activity) throws BusinessException {
		return activityService.addActivity(activity);
	}

	public Activity disableActivity(int activityId) throws BusinessException {
		return activityService.disableActivity(activityId);
	}

	public Activity editActivity(Activity newActivity) throws BusinessException {
		return activityService.editActivity(newActivity);
	}

}
