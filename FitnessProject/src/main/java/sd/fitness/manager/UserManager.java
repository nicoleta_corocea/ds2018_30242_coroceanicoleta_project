package sd.fitness.manager;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sd.fitness.config.exception.BusinessException;
import sd.fitness.model.User;
import sd.fitness.model.security.LoginCredentialsDTO;
import sd.fitness.service.UserService;

@Service
public class UserManager {

	@Autowired
	private UserService userService;

	public List<User> getAllUsers() {
		return userService.getAllUsers();
	}

	public User insertUser(User user) throws BusinessException {
		return userService.insertUser(user);
	}

	public User login(LoginCredentialsDTO loginCredentialsDTO) throws BusinessException {
		return userService.login(loginCredentialsDTO);
	}

	public User changeStatus(Integer id, String status) throws BusinessException {
		return userService.changeStatus(id, status);
	}

	public List<User> getActiveUsersByRole(String role) throws BusinessException {
		return userService.getActiveUsersByRole(role);
	}

	public boolean hasValidSubscription(int userId) {
		return userService.hasValidSubscription(userId);
	}
	public List<User> getUsersWithValidSubscription(){
		List<User> users =new ArrayList<>();
		List<User> allUsers= userService.getAllUsers();
		for (User u: allUsers){
			if (hasValidSubscription(u.getId()) && !u.getStatus().equals("DELETED")){
				users.add(u);
			}
		}
		return users;
	}

	public User edit(User updateUser) throws BusinessException {
		return userService.edit(updateUser);
	}

	public Set<User> getTrainers(Integer userId, Boolean hasAppointment) throws BusinessException {
		return userService.getTrainers(userId, hasAppointment);
	}

	public User getUserById(int id) throws BusinessException {
		return userService.getUserById(id);
	}
}
