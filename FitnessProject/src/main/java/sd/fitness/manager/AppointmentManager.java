package sd.fitness.manager;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sd.fitness.config.exception.BusinessException;
import sd.fitness.model.Activity;
import sd.fitness.model.Appointment;
import sd.fitness.model.Subscription;
import sd.fitness.service.ActivityService;
import sd.fitness.service.AppointmentService;
import sd.fitness.service.SubscriptionService;

@Service
public class AppointmentManager {

	@Autowired
	private AppointmentService appointmentService;

	@Autowired
	private ActivityService activityService;

	@Autowired
	private SubscriptionService subscriptionService;

	public Appointment addAppointment(int userId, int activityId) throws BusinessException {
		if (!activityService.hasSeats(activityId)) {
			throw new BusinessException(412, "The activity does not have seats anymore.");
		}

		if (!activityService.isAvailable(activityId)) {
			throw new BusinessException(412, "The activity is not available.");
		}

		if (!hasValidSubscription(userId, activityId)) {
			throw new BusinessException(412, "Expired Subscription");
		}

		Appointment appointment = appointmentService.addAppointment(userId, activityId);
		Activity activity = activityService.decrementRemainingSeats(activityId);
		return appointment;
	}

	public Appointment deleteAppointment(int activityId, int userId) throws BusinessException {
		return appointmentService.deleteAppointment(activityId, userId);
	}

	public List<Appointment> getAllAppointments() {
		return appointmentService.getAllAppointments();
	}

	private boolean hasValidSubscription(int userId, int activityId) throws BusinessException {

		Subscription subscription = subscriptionService.getSubscriptionById(userId);
		Activity activity = activityService.getActivityById(activityId);
		if ((subscription.getExpirationDate() - activity.getStartDate() > 0) && (activity.getStartDate() - subscription.getActivationDate() > 0 )) {
			return true;
		} else {
			return false;
		}
	}

}
