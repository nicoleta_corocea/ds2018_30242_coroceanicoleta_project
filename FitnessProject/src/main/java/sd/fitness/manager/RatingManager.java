package sd.fitness.manager;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sd.fitness.config.exception.BusinessException;
import sd.fitness.model.*;
import sd.fitness.service.ActivityService;
import sd.fitness.service.RatingService;
import sd.fitness.service.UserService;
import sd.fitness.validator.RatingValidator;

@Service
public class RatingManager {

	@Autowired
	private RatingService ratingService;

	@Autowired
	private UserService userService;

	@Autowired
	private ActivityService activityService;

	@Autowired
	private RatingValidator ratingValidator;

	// TODO:: TEST
	public Rating giveRating(Rating rating) throws BusinessException {

		ratingValidator.checkValid(rating);

		User trainer = userService.getUserById(rating.getRatingId().getTrainerId());

		List<Activity> oldActivies = activityService.selectOldActivitiesOfUser(rating.getRatingId().getUserId());
		Set<User> trainers = new HashSet<>();
		for (Activity a : oldActivies) {
			trainers.add(a.getTrainer());
		}

		if (!trainers.contains(trainer)) {
			throw new BusinessException(403, "The user can't give rating to a trainer he never met.");
		}

		return ratingService.giveRating(rating);
	}

	public double getRatingByTrainerId(int trainerId) throws BusinessException {
		User trainer = userService.getUserById(trainerId);
		return computeRating(ratingService.getRatingByTrainerId(trainerId));
	}

	public Map<Integer, Double> getComputedRatings() throws BusinessException {
//		return ratingService.getComputedRatings();

		Map<Integer, Double> results = new HashMap<>();
		Map<Integer, List<Rating>> ratingsForTrainer = new HashMap<>();

		List<Rating> ratings = ratingService.getAllRatings();

		for (Rating r : ratings) {
			if (ratingsForTrainer.containsKey(r.getRatingId().getTrainerId())) {
				ratingsForTrainer.get(r.getRatingId().getTrainerId()).add(r);
			} else {
				List<Rating> newEntry = new ArrayList<>();
				newEntry.add(r);
				ratingsForTrainer.put(r.getRatingId().getTrainerId(), newEntry);
			}
		}
		for (int key : ratingsForTrainer.keySet()) {
			results.put(key, computeRating(ratingsForTrainer.get(key)));
		}

		//add unrated trainers


		Set<User> trainers = userService.getTrainers(0,null);
		for( User t : trainers){
			if(!results.containsKey(t.getId())){
				results.put(t.getId(),0.0);
			}
		}


		return results;
	}

	private double computeRating(List<Rating> ratings) {
		double mediumRating = 0.0;

		if (ratings.isEmpty()) {
			return mediumRating;
		}
		for (Rating r : ratings) {
			mediumRating += r.getRate();
		}
		mediumRating /= ratings.size();
		return mediumRating;

	}

	public List<Rating> getAllRatings() {
		return ratingService.getAllRatings();
	}

	public Rating getRatingByUserIdAndTrainerId(Integer userId, Integer trainerId) throws BusinessException {
		return ratingService.getRatingByUserIdAndTrainerId(userId, trainerId);
	}

	public Rating deleteRating(int userId, int trainerId) throws BusinessException {
		return ratingService.deleteRating(userId, trainerId);
	}

	public Rating editRating(Rating rating) throws BusinessException {
		ratingValidator.checkValid(rating);
		return ratingService.editRating(rating);
	}

}
