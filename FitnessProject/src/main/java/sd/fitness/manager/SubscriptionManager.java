package sd.fitness.manager;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sd.fitness.config.exception.BusinessException;
import sd.fitness.model.Role;
import sd.fitness.model.RoleType;
import sd.fitness.model.Subscription;
import sd.fitness.model.User;
import sd.fitness.service.SubscriptionService;
import sd.fitness.service.UserService;
import sd.fitness.validator.SubscriptionValidator;

@Service
public class SubscriptionManager {

	@Autowired
	private UserService userService;

	@Autowired
	private SubscriptionService subscriptionService;

	@Autowired
	private SubscriptionValidator subscriptionValidator;

	public Subscription addSubscription(Subscription subscription) throws BusinessException {

		subscriptionValidator.checkValid(subscription);

		User user = userService.getUserById(subscription.getId());

		boolean isClient = false;
		for (Role r : user.getRoles()) {
			if (r.getRole().equals(RoleType.CLIENT.toString())) {
				isClient = true;
			}
		}
		if (!isClient) {
			throw new BusinessException(409, "This user type does not support subscription.");
		}
		subscription.setExpirationDate(subscription.getActivationDate() + 30 * 86400);
		subscription.setDisabled(false);
		Subscription savedSubscription = subscriptionService.addSubscription(subscription);

		user.setSubscription(savedSubscription);
		userService.edit(user);

		return savedSubscription;
	}

	public Subscription getSubscriptionById(int subscriptionId) throws BusinessException {
		return subscriptionService.getSubscriptionById(subscriptionId);
	}

//TODO: to be checked, modified
	public Subscription disableSubscription(int idSubscription) throws BusinessException {

		return subscriptionService.disableSubscription(idSubscription);
	}

	public List<Subscription> getAllSubscriptions() {

		return subscriptionService.getAllSubscriptions();
	}

	public Subscription editSubscription(Subscription subscription) throws BusinessException {
		subscriptionValidator.checkValid(subscription);
		return subscriptionService.editSubscription(subscription);
	}

}
