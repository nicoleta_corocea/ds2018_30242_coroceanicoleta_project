drop database fitnessDB;
create database fitnessDB;

use fitnessDB;

CREATE TABLE subscriptions (
	id int,
    activation_date long not null,
    expiration_date long not null,
    subscription_type varchar(20) not null,
    disabled boolean not null,
    PRIMARY KEY (id)
);

CREATE TABLE users (
    id int auto_increment,
    first_name varchar(20) NOT NULL,
    last_name varchar(20) NOT NULL,
    email varchar(30) NOT NULL,
    password varchar(256) NOT NULL,
    id_subscription int,
    status varchar(10) not null,
    PRIMARY KEY (id), 
    foreign key (id_subscription) references subscriptions(id)
);

CREATE TABLE roles (
	id int,
    role varchar(15) not null,
    PRIMARY KEY (id)
);

CREATE TABLE user_role (
	id_user int not null,
    id_role int not null,
    PRIMARY KEY(id_user, id_role),
    FOREIGN KEY (id_user) REFERENCES users(id),
    FOREIGN KEY (id_role) REFERENCES roles(id)
);

CREATE TABLE activities (
	id int auto_increment,
    id_trainer int,
    title varchar(20) not null,
    start_date long not null,
    duration long not null,
    available_seats int not null,
    remaining_seats int not null,
    available boolean not null,
    type varchar(30) not null,
    FOREIGN KEY (id_trainer) REFERENCES users(id),
    primary key(id)
);

CREATE TABLE appointments (
    id_activity int not null,
    id_user int not null,
	FOREIGN KEY (id_activity) REFERENCES activities(id),
	FOREIGN KEY (id_user) REFERENCES users(id),
    primary key(id_activity, id_user)
);

CREATE TABLE ratings (
	id_user int not null,
    id_trainer int not null,
    rate float not null,
    PRIMARY KEY(id_user, id_trainer),
    FOREIGN KEY (id_user) REFERENCES users(id),
    FOREIGN KEY (id_trainer) REFERENCES users(id)
);

INSERT INTO `roles` (`id`,`role`) VALUES (1, 'ADMIN'); 
INSERT INTO `roles` (`id`,`role`) VALUES (2, 'TRAINER'); 
INSERT INTO `roles` (`id`,`role`) VALUES (3, 'CLIENT'); 

INSERT INTO `users` (`first_name`,`last_name`,`email`,`password`,`status`) VALUES ('admin','admin','admin@admin.com','admin',''); 
INSERT INTO `user_role` (`id_user`,`id_role`) VALUES (1,1);

INSERT INTO `subscriptions` (`id`,`activation_date`,`expiration_date`,`subscription_type`,`disabled`) VALUES (2,1543968000,1546646400,'VIP',FALSE); 
INSERT INTO `users` (`first_name`,`last_name`,`email`,`password`,`id_subscription`,`status`) VALUES  ('Nicoleta','Corocea','nicoleta_corocea@yahoo.com','parola',2,'ACTIVE'); 
INSERT INTO `users` (`first_name`,`last_name`,`email`,`password`,`status`) VALUES ('Adrian','Popescu','nicoletacorocea@gmail.com','parola1','ACTIVE'); 


INSERT INTO `user_role` (`id_user`,`id_role`) VALUES (2,3);
INSERT INTO `user_role` (`id_user`,`id_role`) VALUES (3,2);

# positive test:

INSERT INTO `activities` (`id_trainer`, `title`, `start_date`, `duration`, `available_seats`, `remaining_seats`,`available`, `type`) VALUES (3,'zumba',1543536000, 30, 30, 20,true, 'CLASS');
INSERT INTO `appointments` (`id_activity`,`id_user`) VALUES (1,2);
