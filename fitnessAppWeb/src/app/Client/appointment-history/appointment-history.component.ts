import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatSnackBar, MatSort, MatTableDataSource} from '@angular/material';
import {Activity, ActivityControllerService, AppointmentControllerService} from '../../shared/api-models';

@Component({
  selector: 'app-appointment-history',
  templateUrl: './appointment-history.component.html',
  styleUrls: ['./appointment-history.component.css'],
  providers: [AppointmentControllerService, ActivityControllerService]
})
export class AppointmentHistoryComponent implements OnInit {
  @ViewChild(MatSort) sort: MatSort;
  activities: Activity[] = [];
  displayedColumns: string[] = ['trainerName','title', 'startDate', 'duration','availableSeats','remainingSeats','available','type'];

  constructor(private activityService: ActivityControllerService,private appointmentService: AppointmentControllerService, private snackBar: MatSnackBar,
              public dialog: MatDialog) { }
  dataSource : MatTableDataSource<Activity>;
  ngOnInit() {

    this.getMyActivities();
  }
  getMyActivities(){
    this.activities = [];
    this.activityService.getAllActivitiesUsingGET(false)
      .subscribe(
        response => {
          console.log("bine");

          let ok = true;
          for(let a in response){
            ok =false;
            for( let u in response[a].participants){
              if(response[a].participants[u].id == JSON.parse(localStorage.getItem("userId") ))
              {
                ok=true;
              }
            }
            if (ok && response[a].available){
              this.activities.push(response[a]);
            }
          }

          this.afterResponse();
        },
        error => {
          console.log("eroare?");
          this.openSnackBar(error.error.message, "Close")
        }
      );
  }
  afterResponse(){

    this.dataSource = new MatTableDataSource(this.activities);
    setTimeout(() => {
      this.dataSource.sort = this.sort;});
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }

  addAppointment(id: any) {
    this.appointmentService.addAppointmentUsingPOST(JSON.parse(localStorage.getItem("userId")),id)
      .subscribe(
        response => {
          console.log("bine");
          this.openSnackBar("Appointment added with success", "Close");
          this.getMyActivities();
          this.afterResponse();
        },
        error => {
          console.log("eroare?");
          this.openSnackBar(error.error.message, "Close")
        }
      );
  }
}
