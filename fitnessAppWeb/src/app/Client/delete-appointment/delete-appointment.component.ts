import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatSnackBar, MatSort, MatTableDataSource} from '@angular/material';
import {Activity, ActivityControllerService, AppointmentControllerService} from '../../shared/api-models';

@Component({
  selector: 'app-delete-appointment',
  templateUrl: './delete-appointment.component.html',
  styleUrls: ['./delete-appointment.component.css'],
  providers: [AppointmentControllerService, ActivityControllerService]
})
export class DeleteAppointmentComponent implements OnInit {
  @ViewChild(MatSort) sort: MatSort;
  activities: Activity[] = [];
  displayedColumns: string[] = ['trainerName','title', 'startDate', 'duration','availableSeats','remainingSeats','available','type',"deleteAppointment"];

  constructor(private activityService: ActivityControllerService,private appointmentService: AppointmentControllerService, private snackBar: MatSnackBar,
              public dialog: MatDialog) { }
  dataSource : MatTableDataSource<Activity>;
  ngOnInit() {

    this.getMyActivities();
  }
  getMyActivities(){
    this.activities = [];
    this.activityService.getAllActivitiesUsingGET(true)
      .subscribe(
        response => {
          console.log("bine");


          for(let a in response){
            for( let u in response[a].participants){
              if(response[a].participants[u].id == JSON.parse(localStorage.getItem("userId") ))
              {
                if (response[a].available){
                  this.activities.push(response[a]);
                }
              }
            }

          }

          this.afterResponse();
        },
        error => {
          console.log("eroare?");
          this.openSnackBar(error.error.message, "Close")
        }
      );
  }
  afterResponse(){

    this.dataSource = new MatTableDataSource(this.activities);
    setTimeout(() => {
      this.dataSource.sort = this.sort;});
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }

  deleteAppointment(id: any) {
    this.appointmentService.deleteAppointmentUsingDELETE(id, JSON.parse(localStorage.getItem("userId")))
      .subscribe(
        response => {
          console.log("bine");
          this.openSnackBar("Appointment deleted with success", "Close");
          this.afterResponse();
          this.getMyActivities();
        },
        error => {
          console.log("eroare?");
          this.openSnackBar(error.error.message, "Close")
        }
      );
  }
}
