import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-client-home',
  templateUrl: './client-home.component.html',
  styleUrls: ['./client-home.component.css']
})
export class ClientHomeComponent implements OnInit {
  constructor(private route: Router, private snackBar: MatSnackBar) { }

  ngOnInit() {
    if(localStorage.getItem("userId")==null || localStorage.getItem("currentUser")==null){
      localStorage.clear();
      this.openSnackBar("Your action is unauthorized. Please login first.", "Close");
      this.route.navigate(['/login']);
    }

    let user = JSON.parse(localStorage.getItem("currentUser"));
    if(user.roles[0].role !='CLIENT'){
      switch (user.roles[0].role) {
        case 'ADMIN':{
          this.openSnackBar("Your cannot access client dashboard.", "Close");
          this.route.navigate(['/admin']);
          break;
        }
        case 'TRAINER':{
          this.openSnackBar("Your cannot access client dashboard.", "Close");
          this.route.navigate(['/trainer']);
          break;
        }
        default:{
          this.openSnackBar("Your action is unauthorized. Please login first.", "Close");
          localStorage.clear();
          this.route.navigate(['/login']);
          break;
        }
      }

    }
  }

  message:string;

  receiveMessage($event) {
    this.message = $event;
    console.log(this.message + " este primit");
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
    });
  }
}
