import {Component, OnInit, ViewChild} from '@angular/core';
import {
  Activity,
  ActivityControllerService,
  AppointmentControllerService,
  RatingControllerService,
  User,
  UserControllerService
} from '../../shared/api-models';
import {MatDialog, MatSnackBar, MatSort, MatTableDataSource} from '@angular/material';

interface IRating {
  userFirstName: string;
  userLastName: string;
  id: number;
  rating: number;
}

@Component({
  selector: 'app-all-trainers',
  templateUrl: './all-trainers.component.html',
  styleUrls: ['./all-trainers.component.css'],
  providers: [RatingControllerService, UserControllerService]
})
export class AllTrainersComponent implements OnInit {

  data=[];
  constructor(private ratingService: RatingControllerService,private userService: UserControllerService,private snackBar: MatSnackBar) { }
  displayedColumns: string[] = ['trainerName','rating'];
  dataSource : MatTableDataSource<IRating>;
  @ViewChild(MatSort) sort: MatSort;

  allRatings;
  currentRate: number;
  ngOnInit() {
    this.currentRate = 7;
    this.ratingService.getAllComputedRatingsUsingGET()
      .subscribe(
        response => {
          console.log("bine");

          this.allRatings = response;
          this.computeRatings();
        },
        error => {
          console.log("eroare?");
          this.openSnackBar(error.error.message, "Close")
        }
      );


  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }

  private computeRatings() {
    let i = 0;
    for(let r in this.allRatings){
      this.userService.getUserByIDUsingGET(parseInt(r))
        .subscribe(
          response => {
            console.log("bine");
            let elem : IRating = {
              userFirstName:response.firstName,
              userLastName:response.lastName,
              id:response.id,
              rating:parseInt(this.allRatings[r])
            };
            this.data.push(elem)
            i++;
            if(i==Object.keys(this.allRatings).length){
              this.afterResponse();
            }
            //this.computeRatings();
          },
          error => {
            console.log("eroare?");
            this.openSnackBar(error.error.message, "Close")
          }
        );
    }
  }

  afterResponse(){

    this.dataSource = new MatTableDataSource(this.data);
    setTimeout(() => {
      this.dataSource.sort = this.sort;});
  }

}
