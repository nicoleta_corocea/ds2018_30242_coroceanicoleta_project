import {Component, OnInit, ViewChild} from '@angular/core';
import {
  Activity,
  ActivityControllerService,
  AppointmentControllerService, Rating,
  RatingControllerService, RatingId,
  User,
  UserControllerService
} from '../../shared/api-models';
import {MatDialog, MatSnackBar, MatSort, MatTableDataSource} from '@angular/material';

interface IRating {
  userFirstName: string;
  userLastName: string;
  id: number;
  rating: number;
}

@Component({
  selector: 'app-give-rating',
  templateUrl: './give-rating.component.html',
  styleUrls: ['./give-rating.component.css'],
  providers: [RatingControllerService, UserControllerService]
})
export class GiveRatingComponent implements OnInit {

  data=[];
  constructor(private ratingService: RatingControllerService,private userService: UserControllerService,private snackBar: MatSnackBar) { }
  displayedColumns: string[] = ['trainerName','rating'];
  dataSource : MatTableDataSource<IRating>;
  @ViewChild(MatSort) sort: MatSort;

  allTrainers;
  allRatings;
  currentRate: number;
  ngOnInit() {
    this.currentRate = 7;
    this.userService.getTrainersUsingGET(parseInt(JSON.parse(localStorage.getItem("userId"))),true)
      .subscribe(
        response => {
          console.log("bine");

          this.allTrainers = response;
          this.computeRatings();
        },
        error => {
          console.log("eroare?");
          this.openSnackBar(error.error.message, "Close")
        }
      );


  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }

  private computeRatings() {
    let i = 0;
    this.ratingService.getAllComputedRatingsUsingGET()
      .subscribe(
        response => {
          this.allRatings = response;
          this.afterResponse();
        },
        error => {
          console.log("eroare?");
          this.openSnackBar(error.error.message, "Close")
        }
      );
  }

  afterResponse(){

    this.data = [];
    for(let i in this.allTrainers){
      let elem : IRating = {
        userFirstName:this.allTrainers[i].firstName,
        userLastName:this.allTrainers[i].lastName,
        id:this.allTrainers[i].id,
        rating:this.allRatings[this.allTrainers[i].id]
      };
      this.data.push(elem)
    }

    this.dataSource = new MatTableDataSource(this.data);
    setTimeout(() => {
      this.dataSource.sort = this.sort;});
  }


  updateClientRatings(id: any, rating: number | any) {
    console.log(id + "" +rating);
    let newRating : Rating = {
      ratingId: {
        userId: parseInt(JSON.parse(localStorage.getItem("userId"))),
        trainerId: id
      },
      rate: rating
    };
    // call api to add rating + search in data dupa idTrainer si schimba rating cu cel primit de la server
    this.ratingService.giveRatingUsingPOST(newRating)
      .subscribe(
        response => {
          this.allRatings = response;
          // this.afterResponse();
          this.openSnackBar("Rating registered successfully", "Close");
          this.ngOnInit();
        },
        error => {
          console.log("eroare?");
          this.openSnackBar(error.error.message, "Close");
          this.ngOnInit();
        }
      );
  }

}
