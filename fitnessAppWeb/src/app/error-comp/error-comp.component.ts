import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-error-comp',
  templateUrl: './error-comp.component.html',
  styleUrls: ['./error-comp.component.css']
})
export class ErrorCompComponent implements OnInit {

  constructor(private route: Router) { }

  ngOnInit() {
  }

  goToLoginPage() {
    this.route.navigate(['/admin']);
  }
}
