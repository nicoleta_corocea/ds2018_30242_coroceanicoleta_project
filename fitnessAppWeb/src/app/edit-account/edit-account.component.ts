import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {MatButton, MatSnackBar} from '@angular/material';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Role, User, UserControllerService} from '../shared/api-models';

@Component({
  selector: 'app-edit-account',
  templateUrl: './edit-account.component.html',
  styleUrls: ['./edit-account.component.css'],
  providers: [UserControllerService]
})
export class EditAccountComponent implements OnInit, AfterViewInit {
  @ViewChild(MatButton) submitButton: MatButton;

  user: User;
  editAccountForm: FormGroup;
  constructor(private userService: UserControllerService, private snackBar: MatSnackBar) {
  }

  ngOnInit() {
    this.editAccountForm = new FormGroup({
      firstName: new FormControl('', Validators.required),
      lastName: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required)
    });

  }

  getCurrentUser(){
    this.user = JSON.parse(localStorage.getItem("currentUser"));
    this.setDataToViews();
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }

  setDataToViews(){
    this.editAccountForm.patchValue({
      firstName: this.user.firstName,
      lastName: this.user.lastName,
      password: this.user.password
    });
  }


  editAccount() {
    this.user.password = this.editAccountForm.value.password;
    this.user.firstName = this.editAccountForm.value.firstName;
    this.user.lastName = this.editAccountForm.value.lastName;



    this.userService.editUsingPUT(this.user)
      .subscribe(
        response => {
          console.log("bine");
          this.openSnackBar("Account edited", "Close");
          localStorage.setItem('currentUser', JSON.stringify(response));
        },
        error => {
          console.log("eroare?");
          this.openSnackBar(error.error.message, "Close")
        }
      );


  }

  ngAfterViewInit(): void {
    setTimeout(() => this.getCurrentUser(), 0);

  }
}
