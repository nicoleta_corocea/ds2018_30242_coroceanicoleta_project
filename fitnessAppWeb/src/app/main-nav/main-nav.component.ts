import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router} from '@angular/router';
import {send} from 'q';

@Component({
  selector: 'app-main-nav',
  templateUrl: './main-nav.component.html',
  styleUrls: ['./main-nav.component.css']
})
export class MainNavComponent {
  @Output() messageEvent = new EventEmitter<string>();
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );
  private clientItems;
  private adminItems;
  private trainerItems;
  private location = '';
  userName: any;
  constructor(private breakpointObserver: BreakpointObserver, private route: Router) {
    this.userName = JSON.parse(localStorage.getItem("currentUser")).firstName;
    this.location = route.url;
    this.adminItems = [
      {
        label: 'Users',
        faIcon: 'fab fa-500px',
        items: [
          {
            label: 'Add User',
            link: '/addUser'
          },
          {
            label: 'Disable User',
            link: '/changeStatusDisable'
          }
          ,
          {
            label: 'Enable User',
            link: '/changeStatusEnable'
          }
          ,
          {
            label: 'See all Users',
            link: '/seeUsers'
          }
        ]
      },
      {
        label: 'Subscriptions',
        items: [
          {
            label: 'Create Subscription',
            link: '/createSubscription'
          },
          {
            label: 'Disable Subscription',
            link: '/disableSubscription'
          },
          {
            label: 'All Subscriptions',
            link: '/allSubscriptions'
          }
        ]
      },
      {
        label: 'Lifetime Activities',
        link: '/allActivities'
      }
    ];
    this.clientItems = [
      {
        label: 'Trainers',
        items: [
          {
            label: 'All Trainers',
            link: '/allTrainers'
          }
          ,{
            label: 'Rate your trainers',
            link: '/rating'
          }]}
      ,{
        label: 'Available Activities',
        link: '/activities'
      }
      ,{
        label: 'Activities History',
        link: '/activitiesHistory'
      }
      ,{
        label: 'Your Appointments',
        link: '/appointments'
      },
      {
        label: 'Edit Account',
        link: '/editAccount'
      }];
    this.trainerItems = [{
      label: 'Activities',
      items:[
        {
          label: 'Add Activity',
          link: '/addActivity'
        },
        {
          label: 'My Activities',
          link: '/myActivities'
        },

      ]
    },
      {
        label: 'Edit Account',
        link: '/editAccount'
      }];

  }
  selectedItem(value) {
    console.log(value);
    this.sendMessage(value.link);
  }
  sendMessage(message : string) {
    this.messageEvent.emit(message)
  }


  logout() {
    if(localStorage.getItem("userId")!=null) {
      localStorage.removeItem('userId');
    }
    if(localStorage.getItem("currentUser")!=null) {
      localStorage.removeItem('currentUser');;
    }

    this.route.navigate(['/login']);
  }
}
