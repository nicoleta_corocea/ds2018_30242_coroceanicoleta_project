import { Component, OnInit, ViewChild } from '@angular/core';
import {MatProgressBar, MatButton, MatSnackBar} from '@angular/material';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import {Router} from '@angular/router';
import {LoginCredentialsDTO, Role, UserControllerService} from '../shared/api-models';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [UserControllerService]
})
export class LoginComponent implements OnInit {
  @ViewChild(MatProgressBar) progressBar: MatProgressBar;
  @ViewChild(MatButton) submitButton: MatButton;

  signinForm: FormGroup;
  serverErrorMessage: String;
  constructor( private userService: UserControllerService, private route : Router, private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.signinForm = new FormGroup({
      username: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required),
    });
  }

  signin() {
    const signinData = this.signinForm.value;
    this.submitButton.disabled = true;
    this.progressBar.mode = 'indeterminate';

    let credentials : LoginCredentialsDTO = {
      email : signinData.username,
      password : signinData.password
    }
    this.userService.loginUsingPOST(credentials)
      .subscribe(
        response => {
          console.log("bine");
          this.submitButton.disabled=false;
          this.progressBar.mode='determinate';
          localStorage.setItem('currentUser', JSON.stringify(response));
          localStorage.setItem('userId',JSON.stringify(response.id));
          let role :Role;
          role = response.roles[0];
          if (role.role == 'ADMIN') {
            this.route.navigate(['/admin'])
          }
          if (role.role == 'CLIENT') {
            this.route.navigate(['/client'])
          }
          if (role.role == 'TRAINER') {
            this.route.navigate(['/trainer'])
          }

          //
        },
        error => {
          console.log("eroare?");
          this.submitButton.disabled=false;
          this.progressBar.mode='determinate';
          this.openSnackBar(error.error.message, "Close")
        }
      );
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }
}
