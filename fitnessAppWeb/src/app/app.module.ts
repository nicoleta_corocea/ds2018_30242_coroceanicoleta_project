import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule} from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { MainNavComponent } from './main-nav/main-nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { NgMaterialMultilevelMenuModule } from 'ng-material-multilevel-menu';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';

import {
  MatToolbarModule,
  MatButtonModule,
  MatSidenavModule,
  MatIconModule,
  MatListModule,
  MatProgressBarModule,
  MatFormFieldModule, MatCheckboxModule, MatCardModule, MatInputModule,
  MatSnackBarModule,
  MatOptionModule,
  MatSelectModule, MatButtonToggleModule, MatRadioButton, MatSlideToggleModule,
  MatSortModule,
  MatTableModule,
  MatDatepickerModule,
  MatNativeDateModule
} from '@angular/material';
import { AdminHomeComponent } from './Admin/admin-home/admin-home.component';
import { ClientHomeComponent } from './Client/client-home/client-home.component';
import { TrainerHomeComponent } from './Trainer/trainer-home/trainer-home.component';
import { AlertComponent } from './alert/alert.component';
import { HttpClientModule } from '@angular/common/http';
import {ActivityControllerService, UserControllerService} from './shared/api-models';
import { AddUserComponent } from './Admin/add-user/add-user.component';
import { DisableUserComponent } from './Admin/disable-user/disable-user.component';
import { EnableUserComponent } from './Admin/enable-user/enable-user.component';
import { SeeAllUsersComponent } from './Admin/see-all-users/see-all-users.component';
import { CreateSubscriptionComponent } from './Admin/create-subscription/create-subscription.component';
import { DisableSubscriptionComponent } from './Admin/disable-subscription/disable-subscription.component';
import { AllSubscriptionsComponent } from './Admin/all-subscriptions/all-subscriptions.component';
import { AllActivitiesComponent } from './Admin/all-activities/all-activities.component';
import { AddActivityComponent } from './Trainer/add-activity/add-activity.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {DeleteActivityDialog, DialogOverviewExampleDialog, MyActivitiesComponent} from './Trainer/my-activities/my-activities.component';
import { EditAccountComponent } from './edit-account/edit-account.component';
import { AddAppointmentComponent } from './Client/add-appointment/add-appointment.component';
import { DeleteAppointmentComponent } from './Client/delete-appointment/delete-appointment.component';
import { AllTrainersComponent } from './Client/all-trainers/all-trainers.component';
import { GiveRatingComponent } from './Client/give-rating/give-rating.component';
import { ErrorCompComponent } from './error-comp/error-comp.component';
import { AppointmentHistoryComponent } from './Client/appointment-history/appointment-history.component';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MainNavComponent,
    AdminHomeComponent,
    ClientHomeComponent,
    TrainerHomeComponent,
    AlertComponent,
    AddUserComponent,
    DisableUserComponent,
    EnableUserComponent,
    SeeAllUsersComponent,
    CreateSubscriptionComponent,
    DisableSubscriptionComponent,
    AllSubscriptionsComponent,
    AllActivitiesComponent,
    AddActivityComponent,
    MyActivitiesComponent,
    EditAccountComponent,
    DialogOverviewExampleDialog,
    DeleteActivityDialog,
    AddAppointmentComponent,
    DeleteAppointmentComponent,
    AllTrainersComponent,
    GiveRatingComponent,
    ErrorCompComponent,
    AppointmentHistoryComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    NgMaterialMultilevelMenuModule,
    MatProgressBarModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatCheckboxModule,
    MatCardModule,
    MatInputModule,
    HttpClientModule,
    MatSnackBarModule,
    MatOptionModule,
    MatSelectModule,
    MatButtonToggleModule,
    MatSortModule,
    MatTableModule,
    MatDatepickerModule,
    MatNativeDateModule,
    NgbModule
  ],
  providers: [UserControllerService,ActivityControllerService],
  bootstrap: [AppComponent],
  entryComponents: [DialogOverviewExampleDialog, DeleteActivityDialog]
})
export class AppModule { }
