/**
 * Api Documentation
 * Api Documentation
 *
 * OpenAPI spec version: 1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
/* tslint:disable:no-unused-variable member-ordering */

import { Inject, Injectable, Optional }                      from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams,
         HttpResponse, HttpEvent }                           from '@angular/common/http';
import { CustomHttpUrlEncodingCodec }                        from '../encoder';

import { Observable }                                        from 'rxjs';

import { Appointment } from '../model/appointment';

import { BASE_PATH, COLLECTION_FORMATS }                     from '../variables';
import { Configuration }                                     from '../configuration';
import {User} from '..';


@Injectable()
export class AppointmentControllerService {

    protected basePath = 'http://localhost:8080';
    public defaultHeaders = new HttpHeaders();
    public configuration = new Configuration();

    constructor(protected httpClient: HttpClient, @Optional()@Inject(BASE_PATH) basePath: string, @Optional() configuration: Configuration) {
        if (basePath) {
            this.basePath = basePath;
        }
        if (configuration) {
            this.configuration = configuration;
            this.basePath = basePath || configuration.basePath || this.basePath;
        }
    }

    /**
     * @param consumes string[] mime-types
     * @return true: consumes contains 'multipart/form-data', false: otherwise
     */
    private canConsumeForm(consumes: string[]): boolean {
        const form = 'multipart/form-data';
        for (const consume of consumes) {
            if (form === consume) {
                return true;
            }
        }
        return false;
    }


    /**
     * addAppointment
     * 
     * @param userId userId
     * @param activityId activityId
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public addAppointmentUsingPOST(userId: number, activityId: number, observe?: 'body', reportProgress?: boolean): Observable<Appointment>;
    public addAppointmentUsingPOST(userId: number, activityId: number, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<Appointment>>;
    public addAppointmentUsingPOST(userId: number, activityId: number, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<Appointment>>;
    public addAppointmentUsingPOST(userId: number, activityId: number, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        if (userId === null || userId === undefined) {
            throw new Error('Required parameter userId was null or undefined when calling addAppointmentUsingPOST.');
        }

        if (activityId === null || activityId === undefined) {
            throw new Error('Required parameter activityId was null or undefined when calling addAppointmentUsingPOST.');
        }

        let queryParameters = new HttpParams({encoder: new CustomHttpUrlEncodingCodec()});
        if (userId !== undefined && userId !== null) {
            queryParameters = queryParameters.set('userId', <any>userId);
        }
        if (activityId !== undefined && activityId !== null) {
            queryParameters = queryParameters.set('activityId', <any>activityId);
        }

      let currentUser: User;
      currentUser= JSON.parse(localStorage.getItem("currentUser"));
      let headers = this.defaultHeaders.append("Authorization","Basic "+btoa(currentUser.email+":"+ currentUser.password));



      // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            '*/*'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
            'application/json'
        ];

        return this.httpClient.post<Appointment>(`${this.basePath}/appointments/add`,
            null,
            {
                params: queryParameters,
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * deleteAppointment
     * 
     * @param activityId activityId
     * @param userId userId
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public deleteAppointmentUsingDELETE(activityId: number, userId: number, observe?: 'body', reportProgress?: boolean): Observable<Appointment>;
    public deleteAppointmentUsingDELETE(activityId: number, userId: number, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<Appointment>>;
    public deleteAppointmentUsingDELETE(activityId: number, userId: number, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<Appointment>>;
    public deleteAppointmentUsingDELETE(activityId: number, userId: number, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        if (activityId === null || activityId === undefined) {
            throw new Error('Required parameter activityId was null or undefined when calling deleteAppointmentUsingDELETE.');
        }

        if (userId === null || userId === undefined) {
            throw new Error('Required parameter userId was null or undefined when calling deleteAppointmentUsingDELETE.');
        }

        let queryParameters = new HttpParams({encoder: new CustomHttpUrlEncodingCodec()});
        if (activityId !== undefined && activityId !== null) {
            queryParameters = queryParameters.set('activityId', <any>activityId);
        }
        if (userId !== undefined && userId !== null) {
            queryParameters = queryParameters.set('userId', <any>userId);
        }

      let currentUser: User;
      currentUser= JSON.parse(localStorage.getItem("currentUser"));
      let headers = this.defaultHeaders.append("Authorization","Basic "+btoa(currentUser.email+":"+ currentUser.password));



      // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            '*/*'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
        ];

        return this.httpClient.delete<Appointment>(`${this.basePath}/appointments/delete`,
            {
                params: queryParameters,
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * getAllAppointments
     * 
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public getAllAppointmentsUsingGET(observe?: 'body', reportProgress?: boolean): Observable<Array<Appointment>>;
    public getAllAppointmentsUsingGET(observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<Array<Appointment>>>;
    public getAllAppointmentsUsingGET(observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<Array<Appointment>>>;
    public getAllAppointmentsUsingGET(observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            '*/*'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
        ];

        return this.httpClient.get<Array<Appointment>>(`${this.basePath}/appointments/all`,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

}
