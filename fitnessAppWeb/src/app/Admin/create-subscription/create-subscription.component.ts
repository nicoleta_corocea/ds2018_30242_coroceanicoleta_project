import { Component, OnInit } from '@angular/core';
import {Subscription, SubscriptionControllerService, User, UserControllerService} from '../../shared/api-models';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MatSnackBar} from '@angular/material';
import {RoleType} from '../add-user/add-user.component';

export interface SubscriptionType {
  value: string;
  viewValue: string;
}
@Component({
  selector: 'app-create-subscription',
  templateUrl: './create-subscription.component.html',
  styleUrls: ['./create-subscription.component.css'],
  providers: [SubscriptionControllerService, UserControllerService]
})
export class CreateSubscriptionComponent implements OnInit {

  createSubscriptionForm: FormGroup;
  users: User[];
  stypes: SubscriptionType[] = [
    {value: 'VIP', viewValue: 'VIP'},
    {value: 'STANDARD', viewValue: 'STANDARD'},
    {value: 'FITNESS', viewValue: 'FITNESS'},
    {value: 'AEROBIC', viewValue: 'AEROBIC'}
  ];
  date = new Date();
  minDate : Date;
  constructor(private subscriptionService: SubscriptionControllerService,private userService: UserControllerService, private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.minDate = new Date(this.date.getFullYear(),this.date.getMonth(),this.date.getUTCDate());
    this.getActiveClients();
    this.createSubscriptionForm = new FormGroup({
      startDate: new FormControl('', Validators.required),
      userName: new FormControl('', Validators.required),
      subscriptionType: new FormControl('', Validators.required)
    });
  }
  createSubscription(){
    let subscription: Subscription = {
      id: parseInt(this.createSubscriptionForm.value.userName),
      activationDate: this.createSubscriptionForm.value.startDate.getTime()/1000,
      subscriptionType: this.createSubscriptionForm.value.subscriptionType
    }
    this.subscriptionService.addSubscriptionUsingPOST(subscription)
      .subscribe(
        response => {
          console.log("bine");

          this.openSnackBar("Subscription created", "Close")
        },
        error => {
          console.log("eroare?");
          this.openSnackBar(error.error.message, "Close")
        }
      );
  }
  getActiveClients(){
    this.userService.getActiveUsersByRoleUsingGET("CLIENT")
      .subscribe(
        response => {
          console.log("bine");
          this.users = response;
          //
        },
        error => {
          console.log("eroare?");
          this.openSnackBar(error.error.message, "Close")
        }
      );}

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }

}
