import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Role, User, UserControllerService} from '../../shared/api-models';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-enable-user',
  templateUrl: './enable-user.component.html',
  styleUrls: ['./enable-user.component.css'],
  providers: [UserControllerService]
})
export class EnableUserComponent implements OnInit {
  enableUserForm: FormGroup;
  users: User[] =[];

  constructor(private userService: UserControllerService, private snackBar: MatSnackBar) { }
  ngOnInit() {
    this.buildDropDownList();
    this.enableUserForm = new FormGroup({
      userName: new FormControl('', Validators.required)
    });
  }

  buildDropDownList()
  {
    this.users = [];
    this.userService.getAllUsersUsingGET()
      .subscribe(
        response => {
          console.log("bine");
          for(var u in response){
            let s = response[u].roles[0].role;
            if(s != "ADMIN" && response[u].status == "DELETED"){
              this.users.push(response[u]);
            }
          }
          //
        },
        error => {
          console.log("eroare?");
          this.openSnackBar(error.error.message, "Close")
        }
      );
  }

  enableUser(){
    this.userService.changeStatusUsingPOST(parseInt(this.enableUserForm.value.userName),"ACTIVE" )
      .subscribe(
        response => {
          console.log("bine");

          this.openSnackBar("User enabled", "Close")
          this.ngOnInit();
        },
        error => {
          console.log("eroare?");
          this.openSnackBar(error.error.message, "Close")
        }
      );
  }
  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }

  }
