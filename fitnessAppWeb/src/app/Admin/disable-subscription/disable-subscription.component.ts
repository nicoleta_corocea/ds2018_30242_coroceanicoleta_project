import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {SubscriptionControllerService, User, UserControllerService} from '../../shared/api-models';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-disable-subscription',
  templateUrl: './disable-subscription.component.html',
  styleUrls: ['./disable-subscription.component.css'],
  providers: [UserControllerService, SubscriptionControllerService]
})
export class DisableSubscriptionComponent implements OnInit {
  disableSubscriptionForm: FormGroup;
  users: User[];
  constructor(private subscriptionService: SubscriptionControllerService,private userService: UserControllerService, private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.getUsersWithSubscription();
    this.disableSubscriptionForm = new FormGroup({
      userName: new FormControl('', Validators.required)
    });
  }

  getUsersWithSubscription(){
    this.userService.getUsersWithValidSubscriptionUsingGET()
      .subscribe(
        response => {
          console.log("bine");
          this.users = response;
          //
        },
        error => {
          console.log("eroare?");
          this.openSnackBar(error.error.message, "Close")
        }
      );
  }

  disableSubscription(){
    this.subscriptionService.disableSubscriptionUsingPUT(parseInt(this.disableSubscriptionForm.value.userName))
      .subscribe(
        response => {
          console.log("bine");
          this.openSnackBar("Subscription disabled!", "Close")
          this.getUsersWithSubscription();
          //
        },
        error => {
          console.log("eroare?");
          this.openSnackBar(error.error.message, "Close")
        }
      );}

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }
}
