import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DisableSubscriptionComponent } from './disable-subscription.component';

describe('DisableSubscriptionComponent', () => {
  let component: DisableSubscriptionComponent;
  let fixture: ComponentFixture<DisableSubscriptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DisableSubscriptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisableSubscriptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
