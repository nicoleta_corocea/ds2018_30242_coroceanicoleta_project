import {Component, OnInit, ViewChild} from '@angular/core';
import {User, UserControllerService} from '../../shared/api-models';
import {MatSnackBar, MatSort, MatTableDataSource} from '@angular/material';

@Component({
  selector: 'app-all-subscriptions',
  templateUrl: './all-subscriptions.component.html',
  styleUrls: ['./all-subscriptions.component.css']
})
export class AllSubscriptionsComponent implements OnInit {
  users: User[] = [];

  displayedColumns: string[] = ['userName', 'activationDate', 'expirationDate', 'subscriptionType'];
  dataSource : MatTableDataSource<User>;

  @ViewChild(MatSort) sort: MatSort;

  constructor(private userService: UserControllerService, private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.getUsersWithSubscription();
  }

  getUsersWithSubscription(){
    this.userService.getUsersWithValidSubscriptionUsingGET()
      .subscribe(
        response => {
          console.log("bine");
          this.users = response;
          this.afterResponse();
        },
        error => {
          console.log("eroare?");
          this.openSnackBar(error.error.message, "Close")
        }
      );
  }
  afterResponse(){

    this.dataSource = new MatTableDataSource(this.users);
    this.dataSource.sort = this.sort;

  }
  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }
}
