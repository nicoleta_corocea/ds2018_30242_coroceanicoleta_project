import {Component, OnInit, ViewChild} from '@angular/core';
import {AddUserComponent} from '../add-user/add-user.component';
import {MainNavComponent} from '../../main-nav/main-nav.component';
import {Router} from '@angular/router';
import {User} from '../../shared/api-models';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-admin-home',
  templateUrl: './admin-home.component.html',
  styleUrls: ['./admin-home.component.css'],
})
export class AdminHomeComponent implements OnInit {
  constructor( private route: Router, private snackBar: MatSnackBar) { }

  ngOnInit() {
    if(localStorage.getItem("userId")==null || localStorage.getItem("currentUser")==null){
      localStorage.clear();
      this.openSnackBar("Your action is unauthorized. Please login first.", "Close");
      this.route.navigate(['/login']);
    }

    let user = JSON.parse(localStorage.getItem("currentUser"));
    if(user.roles[0].role !='ADMIN'){
      switch (user.roles[0].role) {
        case 'CLIENT':{
          this.openSnackBar("Your cannot access this dashboard.", "Close");
          this.route.navigate(['/client']);
          break;
        }
        case 'TRAINER':{
          this.openSnackBar("Your cannot access this dashboard.", "Close");
          this.route.navigate(['/trainer']);
          break;
        }
        default:{
          this.openSnackBar("Your action is unauthorized. Please login first.", "Close");
          localStorage.clear();
          this.route.navigate(['/login']);
          break;
        }
      }

    }
  }
  message:string;

  receiveMessage($event) {
    this.message = $event;
    console.log(this.message + " este primit");
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
    });
  }
}
