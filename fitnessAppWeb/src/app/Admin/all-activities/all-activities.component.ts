import {Component, OnInit, ViewChild} from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {Activity, ActivityControllerService, SubscriptionControllerService, User, UserControllerService} from '../../shared/api-models';
import {MatSnackBar, MatSort, MatTableDataSource} from '@angular/material';

@Component({
  selector: 'app-all-activities',
  templateUrl: './all-activities.component.html',
  styleUrls: ['./all-activities.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0', display: 'none'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
  providers: [ActivityControllerService]
})
export class AllActivitiesComponent implements OnInit {
  activities: Activity[] = [];

  displayedColumns: string[] = ['trainerName', 'title', 'startDate', 'duration','availableSeats','remainingSeats','available','type'];
  displayedColumnsVersion: string[] = ['userName'];

  dataSource : MatTableDataSource<Activity>;

  @ViewChild(MatSort) sort: MatSort;

  constructor(private activityService: ActivityControllerService, private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.getAllActivities();
  }

  getAllActivities(){
    this.activityService.getAllActivitiesUsingGET()
      .subscribe(
        response => {
          console.log("bine");
          this.activities = response;
          this.afterResponse();
        },
        error => {
          console.log("eroare?");
          this.openSnackBar(error.error.message, "Close")
        }
      );
  }
  afterResponse(){

    this.dataSource = new MatTableDataSource(this.activities);
    this.dataSource.sort = this.sort;

  }
  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }

}
