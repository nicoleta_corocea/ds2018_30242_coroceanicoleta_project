import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {User, UserControllerService} from '../../shared/api-models';
import {MatButton, MatOption, MatProgressBar, MatSelect, MatSnackBar} from '@angular/material';
import {RoleType} from '../add-user/add-user.component';

export interface UserName {
  value: number;
  viewValue: string;
}

@Component({
  selector: 'app-disable-user',
  templateUrl: './disable-user.component.html',
  styleUrls: ['./disable-user.component.css'],
  providers: [UserControllerService]
})
export class DisableUserComponent implements OnInit {
  disableClientForm: FormGroup;
  disableTrainerForm: FormGroup;
  trainers: User[] = [];
  clients: User[] = [];
  constructor(private userService: UserControllerService, private snackBar: MatSnackBar) { }

  getClients(){
      this.clients = [];
      this.userService.getActiveUsersByRoleUsingGET('CLIENT')
      .subscribe(
        response => {
          console.log("bine");
          this.clients = response;

          //
        },
        error => {
          console.log("eroare?");
          this.openSnackBar(error.error.message, "Close");
        }
      );
  }

    getTrainers(){
      this.trainers = [];
      this.userService.getActiveUsersByRoleUsingGET('TRAINER')
      .subscribe(
        response => {
          console.log("bine");
          this.trainers = response;

          // this.disableUserForm.controls['userName'].setValue(this.users);
          //
        },
        error => {
          console.log("eroare?");
          this.openSnackBar(error.error.message, "Close")
        }
      );
  }

  ngOnInit() {
    this.disableClientForm = new FormGroup({
      userName: new FormControl('', Validators.required)
    });
    this.disableTrainerForm = new FormGroup({
      userName: new FormControl('', Validators.required)
    });

    this.getClients();
    this.getTrainers();
  }

  disableTrainer(){

  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }

  disableUser(role: string) {
    let id = -1;
    if(role == 'TRAINER') {
      id = parseInt(this.disableTrainerForm.value.userName);
    }
    if(role == 'CLIENT') {
      id = parseInt(this.disableClientForm.value.userName);
    }
    this.userService.changeStatusUsingPOST(id, "DELETED")
      .subscribe(
        response => {
          console.log("bine");
          this.ngOnInit();
          this.openSnackBar("User disabled", "Close")
        },
        error => {
          console.log("eroare?");
          this.openSnackBar(error.error.message, "Close")
        }
      );
  }
}
