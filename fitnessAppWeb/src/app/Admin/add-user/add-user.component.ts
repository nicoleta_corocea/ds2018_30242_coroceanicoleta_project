import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {LoginCredentialsDTO, Role, User, UserControllerService} from '../../shared/api-models';
import {MatButton, MatButtonToggleGroup, MatProgressBar, MatSnackBar} from '@angular/material';

export interface RoleType {
  value: string;
  viewValue: string;
}
@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css'],
  providers: [UserControllerService]
})


export class AddUserComponent implements OnInit {
  @ViewChild(MatButton) submitButton: MatButton;
  @ViewChild(MatProgressBar) progressBar: MatProgressBar;

  addUserForm: FormGroup;
  toogle: MatButtonToggleGroup;


  roles: RoleType[] = [
    {value: 'CLIENT', viewValue: 'CLIENT'},
    {value: 'TRAINER', viewValue: 'TRAINER'},
  ];
  constructor(private userService: UserControllerService, private snackBar: MatSnackBar) {
  }

  ngOnInit() {
    this.addUserForm = new FormGroup({
      firstName: new FormControl('', Validators.required),
      lastName: new FormControl('', Validators.required),
      email: new FormControl('', Validators.email),
      userType: new FormControl('', Validators.required)
    });
  }

  addUser(){
    this.submitButton.disabled = true;
    this.progressBar.mode = 'indeterminate';
    const addUserData = this.addUserForm.value;

    let role : Role = {
      id: -1,
      role: addUserData.userType
    }
    if(role.role == "CLIENT"){
      role.id = 3;
    }
    else{
      role.id = 2;
    }
    role.role = addUserData.userType;
    let user : User = {
      firstName: addUserData.firstName,
      lastName: addUserData.lastName,
      email: addUserData.email,
      password: "",
      roles: [role]
    }

    this.userService.addMemberUsingPOST(user)
      .subscribe(
        response => {
          console.log("bine");
          this.submitButton.disabled=false;
          this.openSnackBar("User added with success", "Close")
          this.submitButton.disabled = false;
          this.progressBar.mode = 'determinate';
          //
        },
        error => {
          console.log("eroare?");
          this.submitButton.disabled=false;
          this.openSnackBar(error.error.message, "Close")
          this.submitButton.disabled = false;
          this.progressBar.mode = 'determinate';
        }
      );
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }


}
