import {Component, OnInit, ViewChild} from '@angular/core';
import {MatSnackBar, MatSort, MatTableDataSource} from '@angular/material';
import {User, UserControllerService} from '../../shared/api-models';
import {FormGroup} from '@angular/forms';

@Component({
  selector: 'app-see-all-users',
  templateUrl: './see-all-users.component.html',
  styleUrls: ['./see-all-users.component.css'],
  providers: [UserControllerService]
})
export class SeeAllUsersComponent implements OnInit {
  users: User[] = [];

  displayedColumns: string[] = ['firstName', 'lastName', 'email', 'status', 'role'];
  dataSource : MatTableDataSource<User>;

  @ViewChild(MatSort) sort: MatSort;

  constructor(private userService: UserControllerService, private snackBar: MatSnackBar) { }

  ngOnInit() {

    this.buildList();

  }

  buildList()
  {
    this.userService.getAllUsersUsingGET()
      .subscribe(
        response => {
          console.log("bine");
          for(var u in response){
            let s: string;
            s = response[u].roles[0].role;
            if(s != "ADMIN"){
              this.users.push(response[u]);
            }
          }

          this.afterResponse();
          //
        },
        error => {
          console.log("eroare?");
          this.openSnackBar(error.error.message, "Close")
        }
      );
  }

  afterResponse(){

    this.dataSource = new MatTableDataSource(this.users);
    this.dataSource.sort = this.sort;

  }
  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }

}
