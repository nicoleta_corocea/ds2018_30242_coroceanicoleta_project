import {Component, OnInit, ViewChild} from '@angular/core';
import {Activity, ActivityControllerService, User} from '../../shared/api-models';
import {MatButton, MatSnackBar, MatSort, MatTableDataSource} from '@angular/material';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {RoleType} from '../../Admin/add-user/add-user.component';

export interface ActivityType {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-add-activity',
  templateUrl: './add-activity.component.html',
  styleUrls: ['./add-activity.component.css'],
  providers: [ActivityControllerService]
})
export class AddActivityComponent implements OnInit {
  @ViewChild(MatButton) submitButton: MatButton;
  minDate : Date;
  date = new Date();
  addActivityForm: FormGroup;
  @ViewChild(MatSort) sort: MatSort;
  time = {hour: 13, minute: 30};
  activities: Activity[] = [];
  displayedColumns: string[] = ['trainerName', 'title', 'startDate', 'duration','availableSeats','remainingSeats','available','type'];
  activityTypes: ActivityType[] = [
    {value: 'PERSONAL_TRAINING', viewValue: 'PERSONAL TRAINING'},
    {value: 'CLASS', viewValue: 'CLASS'},
  ];
  constructor(private activityService: ActivityControllerService, private snackBar: MatSnackBar) { }
  dataSource : MatTableDataSource<Activity>;
  ngOnInit() {
    this.minDate = new Date(this.date.getFullYear(),this.date.getMonth(),this.date.getUTCDate());
    this.getAllActivities();
    this.addActivityForm = new FormGroup({
      startDate:  new FormControl('', Validators.required),
      duration: new FormControl('',Validators.pattern("^[0-9]*$")),
      title :  new FormControl('', Validators.required),
      availableSeats: new FormControl('',[Validators.pattern("^[0-9]*$"), Validators.required]),
      activityType: new FormControl('', Validators.required)
    });
  }

  getAllActivities(){
    this.activityService.getAllActivitiesUsingGET()
      .subscribe(
        response => {
          console.log("bine");
          this.activities = response;
          this.afterResponse();
        },
        error => {
          console.log("eroare?");
          this.openSnackBar(error.error.message, "Close")
        }
      );
  }
  afterResponse(){

    this.dataSource = new MatTableDataSource(this.activities);
    this.dataSource.sort = this.sort;

  }

  addActivity(){
    const addActivityData = this.addActivityForm.value;
    let trainer : User ={
      id: JSON.parse(localStorage.getItem("userId"))
    }
    let stD=new Date(addActivityData.startDate.getFullYear(),addActivityData.startDate.getMonth(),addActivityData.startDate.getUTCDate()+1,this.time.hour,this.time.minute);
    let activity : Activity = {
      trainer: trainer,
      title: addActivityData.title,
      duration: addActivityData.duration,
      availableSeats: addActivityData.availableSeats,
      remainingSeats: addActivityData.availableSeats,
      type: addActivityData.activityType,
      startDate: stD.getTime()/1000
    }

    this.activityService.addActivityUsingPOST(activity)
      .subscribe(
        response => {
          console.log("bine");
          this.submitButton.disabled=false;
          this.openSnackBar("Activity added with success", "Close")
          this.getAllActivities();
          //
        },
        error => {
          console.log("eroare?");
          this.submitButton.disabled=false;
          this.openSnackBar(error.error.message, "Close")
        }
      );
  }
  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }

}
