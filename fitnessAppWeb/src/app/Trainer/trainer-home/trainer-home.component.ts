import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-trainer-home',
  templateUrl: './trainer-home.component.html',
  styleUrls: ['./trainer-home.component.css']
})
export class TrainerHomeComponent implements OnInit {

  constructor(private route: Router, private snackBar: MatSnackBar) { }

  ngOnInit() {
    if(localStorage.getItem("userId")==null || localStorage.getItem("currentUser")==null){
      localStorage.clear();
      this.openSnackBar("Your action is unauthorized. Please login first.", "Close");
      this.route.navigate(['/login']);
    }

    let user = JSON.parse(localStorage.getItem("currentUser"));
    if(user.roles[0].role !='TRAINER'){
      switch (user.roles[0].role) {
        case 'CLIENT':{
          this.openSnackBar("Your cannot access trainer dashboard.", "Close");
          this.route.navigate(['/client']);
          break;
        }
        case 'ADMIN':{
          this.openSnackBar("Your cannot access trainer dashboard.", "Close");
          this.route.navigate(['/admin']);
          break;
        }
        default:{
          this.openSnackBar("Your action is unauthorized. Please login first.", "Close");
          localStorage.clear();
          this.route.navigate(['/login']);
          break;
        }
      }

    }
  }

  message:string;

  receiveMessage($event) {
    this.message = $event;
    console.log(this.message + " este primit");
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
    });
  }
}
