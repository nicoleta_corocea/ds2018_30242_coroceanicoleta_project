import {AfterViewInit, Component, Inject, OnInit, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatButton, MatDialog, MatDialogRef, MatSnackBar, MatSort, MatTableDataSource} from '@angular/material';
import {Activity, ActivityControllerService, User} from '../../shared/api-models';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivityType} from '../add-activity/add-activity.component';

@Component({
  selector: 'app-my-activities',
  templateUrl: './my-activities.component.html',
  styleUrls: ['./my-activities.component.css'],
  providers: [ActivityControllerService]
})
export class MyActivitiesComponent implements OnInit {
  @ViewChild(MatSort) sort: MatSort;
  private idColumn = 'id';
  activities: Activity[] = [];
  displayedColumns: string[] = ['title', 'startDate', 'duration','availableSeats','remainingSeats','available','type',"editVersion","deleteVersion"];

  constructor(private activityService: ActivityControllerService, private snackBar: MatSnackBar,
    public dialog: MatDialog) { }
  dataSource : MatTableDataSource<Activity>;
  ngOnInit() {

    this.getMyActivities();
  }
  getMyActivities(){
    this.activityService.getActivitiesByUserIdUsingGET(JSON.parse(localStorage.getItem("userId")),null)
      .subscribe(
        response => {
          console.log("bine");
          this.activities = response;

          this.afterResponse();
        },
        error => {
          console.log("eroare?");
          this.openSnackBar(error.error.message, "Close")
        }
      );
  }
  afterResponse(){

    this.dataSource = new MatTableDataSource(this.activities);
    setTimeout(() => {
      this.dataSource.sort = this.sort;});
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }

  editDialog(element): void {

    const dialogRef = this.dialog.open(DialogOverviewExampleDialog, {
      width: '50%',
      data: {element : element}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.getMyActivities();
    });
  }

  deleteDialog(element): void {

    const dialogRef = this.dialog.open(DeleteActivityDialog, {
      width: '30%',
      data: {element : element}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.getMyActivities();
    });
  }

}

//dialog comp
@Component({
  selector: 'edit-activity-dialog',
  templateUrl: 'edit-activity-dialog.html',
})
export class DialogOverviewExampleDialog implements AfterViewInit,OnInit{
  addActivityForm: FormGroup;
  activities: Activity[] = [];
  time = {
    hour: 12,
    minute: 0
  };
  activityTypes: ActivityType[] = [
    {value: 'PERSONAL_TRAINING', viewValue: 'PERSONAL TRAINING'},
    {value: 'CLASS', viewValue: 'CLASS'},
  ];
  @ViewChild(MatButton) submitButton: MatButton;
  constructor(
    public dialogRef: MatDialogRef<DialogOverviewExampleDialog>,
    @Inject(MAT_DIALOG_DATA) public data,
    private activityService: ActivityControllerService, private snackBar: MatSnackBar) {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    this.addActivityForm = new FormGroup({
      startDate:  new FormControl('', Validators.required),
      duration: new FormControl('',Validators.pattern("^[0-9]*$")),
      title :  new FormControl(this.data.title, Validators.required),
      availableSeats: new FormControl('',[Validators.pattern("^[0-9]*$"),Validators.required]),
      activityType: new FormControl('', Validators.required)
    });
    // this.setDataToViews();
  }


  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }

  setDataToViews(){
    let act = this.data.element;

    let d = new Date(act.startDate*1000);
    this.time = {
      hour: d.getHours(),
      minute: d.getMinutes()
    };
    this.addActivityForm.patchValue({
      startDate: new Date(act.startDate*1000),
      duration: act.duration,
      title :  act.title,
      availableSeats: act.availableSeats,
      activityType: act.type
    });
  }

  ngAfterViewInit(): void {
    setTimeout(() => this.setDataToViews(), 0);

  }

  editActivity() {
    const addActivityData = this.addActivityForm.value;
    let trainer : User ={
      id: JSON.parse(localStorage.getItem("userId"))
    }
    let act = this.data.element;
    let stD=new Date(addActivityData.startDate.getFullYear(),addActivityData.startDate.getMonth(),addActivityData.startDate.getUTCDate()+1,this.time.hour,this.time.minute);
    let activity : Activity = {
      id: act.id,
      trainer: trainer,
      title: addActivityData.title,
      duration: addActivityData.duration,
      availableSeats: addActivityData.availableSeats,
      remainingSeats: addActivityData.availableSeats,
      type: addActivityData.activityType,
      startDate: stD.getTime()/1000
    }

    this.activityService.editActivityUsingPUT(activity)
      .subscribe(
        response => {
          console.log("bine");
          this.submitButton.disabled=false;
          this.openSnackBar("Activity edited with success", "Close")
          this.onNoClick();
          //
        },
        error => {
          console.log("eroare?");
          this.submitButton.disabled=false;
          this.openSnackBar(error.error.message, "Close")
        }
      );
  }
}

@Component({
  selector: 'delete-activity-dialog',
  templateUrl: 'delete-activity-dialog.html',
})
export class DeleteActivityDialog implements OnInit{
  deleteActivityForm: FormGroup;
  @ViewChild(MatButton) yesButton: MatButton;
  @ViewChild(MatButton) closeButton: MatButton;
  constructor(
    public dialogRef: MatDialogRef<DeleteActivityDialog>,
    @Inject(MAT_DIALOG_DATA) public data,
    private activityService: ActivityControllerService, private snackBar: MatSnackBar) {
  }
  private act;
  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
   // this.deleteActivityForm = new FormGroup();
    this.act = this.data.element;
  }


  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }


  deleteActivity() {

    this.activityService.disableActivityUsingPUT(this.act)
      .subscribe(
        response => {
          console.log("delete");
          this.openSnackBar("Activity successfully deleted", "Close")
          this.onNoClick();
          //
        },
        error => {
          console.log("eroare?");
          this.openSnackBar(error.error.message, "Close")
          this.onNoClick();
        }
      );
  }

}
