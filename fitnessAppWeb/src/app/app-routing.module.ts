import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from './login/login.component';
import {AdminHomeComponent} from './Admin/admin-home/admin-home.component';
import {TrainerHomeComponent} from './Trainer/trainer-home/trainer-home.component';
import {ClientHomeComponent} from './Client/client-home/client-home.component';
import {ErrorCompComponent} from './error-comp/error-comp.component';

const routes: Routes = [
  {
    path : '',
    component : LoginComponent
  },
  {
    path : 'login',
    component : LoginComponent
  },
  {
    path: 'admin',
    component: AdminHomeComponent
  },
  {
    path: 'client',
    component: ClientHomeComponent
  },
  {
    path: 'trainer',
    component: TrainerHomeComponent
  },
  {
    path: '404',
    component: ErrorCompComponent
  },
  {
    path: '**',
    redirectTo : '404'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
